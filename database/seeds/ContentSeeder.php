<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Content;

class ContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $array = array(
            ['id' => 1,'content'=>'HelpRegister','text'=>null],
            ['id' => 2,'content'=>'คุณสมบัติผู้เข้าสอบ','text'=>null], //KU-EPT HELP
            ['id' => 3,'content'=>'วิธีกรอกข้อมูลในกระดาษคำตอบ','text'=>null], //KU-EPT HELP
            ['id' => 4,'content'=>'แนวทางปฏิบัติสำหรับผู้เข้าสอบ','text'=>null], //KU-EPT HELP
            ['id' => 5,'content'=>'ตัวอย่างข้อสอบ KU-EPT','text'=>null], //KU-EPT HELP
            ['id' => 6,'content'=>'ข้อมูลทั่วไป','text'=>null], //KU-EXITE HELP
            ['id' => 7,'content'=>'เกณฑ์การเทียบคะแนน','text'=>null], //KU-EXITE HELP
            ['id' => 8,'content'=>'ติดตามข่าวสารเพิ่มเติม','text'=>null], //KU-EXITE HELP
            ['id' => 9,'content'=>'กฏระเบียบต่างๆ','text'=>null], //KU-EXITE HELP
            ['id' => 10,'content'=>'คู่มือการสอบ TOEFL ITP','text'=>null], //TOEFL ITP HELP,
            ['id' => 11,'content'=>'About','text'=>null], //About*/
            ['id' => 12,'content'=>'Contact','text'=>null] //Contact
        );
        foreach($array as $row){
            $Content = new Content;
            $Content->ct_id = $row['id'];
            $Content->ct_content = $row['content'];
            $Content->ct_text = $row['text'];
            $Content->save();
        }
    }
}
