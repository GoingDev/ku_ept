<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Category;
class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $array = array(
            ['name' => 'บุคคลทั่วไป'],
            ['name' => 'บุคคลภายใน'],
            ['name' => 'นิสิต / นักศึกษา'],
            ['name' => 'องค์กร / บริษัท'],
        );
        foreach($array as $row){
            $Category = new Category;
            $Category->c_name = $row['name'];
            $Category->save();
        }
    }
}
