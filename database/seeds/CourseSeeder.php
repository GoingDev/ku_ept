<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Course;
class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $array = array(
            [
                'c_id' => 1,
                'course_name' => 'TOEFL ITP 2561/18',
                'course_description' => 'Some Text',
                'course_title' => 'Some Text',
                'course_price' => 1600,
                'course_people' => 90,
                'course_status' => 1,
                'course_img' => 'SomeImg.jpg',
                'course_datestart' => '2018-07-03',
                'course_dateend' => '2018-07-15',
                'course_timestart' => '9:00:00',
                'course_timeend' => '12:00:00'
            ],
            [
                'c_id' => 1,
                'course_name' => 'TOEFL ITP 2561/50,',
                'course_description' => 'Some Text',
                'course_title' => 'Some Text',
                'course_price' => 2000,
                'course_people' => 15,
                'course_status' => 0,
                'course_img' => 'SomeImg.jpg',
                'course_datestart' => '2018-04-02',
                'course_dateend' => '2018-4-09',
                'course_timestart' => '9:00:00',
                'course_timeend' => '15:00:00'
            ],
            [
                'c_id' => 3,
                'course_name' => 'TOEFL ITP 2561/12',
                'course_description' => 'Some Text',
                'course_title' => 'Some Text',
                'course_price' => 1200,
                'course_people' => 40,
                'course_status' => 1,
                'course_img' => 'SomeImg.jpg',
                'course_datestart' => '2018-07-08',
                'course_dateend' => '2018-07-09',
                'course_timestart' => '9:00:00',
                'course_timeend' => '10:30:00'
            ],
            [
                'c_id' => 3,
                'course_name' => 'TOEFL ITP 2561/11,',
                'course_description' => 'Some Text',
                'course_title' => 'Some Text',
                'course_price' => 900,
                'course_people' => 10,
                'course_status' => 0,
                'course_img' => 'SomeImg.jpg',
                'course_datestart' => '2018-07-15',
                'course_dateend' => '2018-07-19',
                'course_timestart' => '17:00:00',
                'course_timeend' => '19:00:00'
            ],
            [
                'c_id' => 3,
                'course_name' => 'TOEFL ITP 2561/9',
                'course_description' => 'Some Text',
                'course_title' => 'Some Text',
                'course_price' => 4000,
                'course_people' => 120,
                'course_status' => 1,
                'course_img' => 'SomeImg.jpg',
                'course_datestart' => '2018-03-03',
                'course_dateend' => '2018-03-30',
                'course_timestart' => '9:00:00',
                'course_timeend' => '18:00:00'
            ],
            [
                'c_id' => 3,
                'course_name' => 'TOEFL ITP 2561/18',
                'course_description' => 'Some Text',
                'course_title' => 'Some Text',
                'course_price' => 1600,
                'course_people' => 90,
                'course_status' => 1,
                'course_img' => 'SomeImg.jpg',
                'course_datestart' => '2018-07-03',
                'course_dateend' => '2018-07-15',
                'course_timestart' => '9:00:00',
                'course_timeend' => '12:00:00'
            ],
            [
                'c_id' => 2,
                'course_name' => 'TOEFL ITP 2561/18',
                'course_description' => 'Some Text',
                'course_title' => 'Some Text',
                'course_price' => 1300,
                'course_people' => 30,
                'course_status' => 1,
                'course_img' => 'SomeImg.jpg',
                'course_datestart' => '2018-09-03',
                'course_dateend' => '2018-09-06',
                'course_timestart' => '9:00:00',
                'course_timeend' => '12:00:00'
            ],
            [
                'c_id' => 2,
                'course_name' => 'TOEFL ITP 2561/33,',
                'course_description' => 'Some Text',
                'course_title' => 'Some Text',
                'course_price' => 1600,
                'course_people' => 90,
                'course_status' => 1,
                'course_img' => 'SomeImg.jpg',
                'course_datestart' => '2018-07-03',
                'course_dateend' => '2018-07-15',
                'course_timestart' => '9:00:00',
                'course_timeend' => '12:00:00'
            ],
            [
                'c_id' => 2,
                'course_name' => 'TOEFL ITP 2563/2,',
                'course_description' => 'Some Text',
                'course_title' => 'Some Text',
                'course_price' => 18050,
                'course_people' => 20,
                'course_status' => 1,
                'course_img' => 'SomeImg.jpg',
                'course_datestart' => '2019-03-03',
                'course_dateend' => '2019-03-13',
                'course_timestart' => '9:00:00',
                'course_timeend' => '12:00:00'
            ],
            [
                'c_id' => 4,
                'course_name' => 'TOEFL ITP 2561/18,',
                'course_description' => 'Some Text',
                'course_title' => 'Some Text',
                'course_price' => 9000,
                'course_people' => 34,
                'course_status' => 0,
                'course_img' => 'SomeImg.jpg',
                'course_datestart' => '2018-12-03',
                'course_dateend' => '2018-12-15',
                'course_timestart' => '9:00:00',
                'course_timeend' => '12:00:00'
            ],
        );
        foreach($array as $row) {
            Course::create($row);
        }
    }
}
