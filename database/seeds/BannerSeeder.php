<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Banner;
class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $array = array(
            ['id' => 1,'name'=>'About','title'=>"เกี่ยวกับเรา",'subtitle'=>null,'img'=>null],//เกี่ยวกับเรา
            ['id' => 2,'name'=>'Course','title'=>"หลักสูตรอบรม",'subtitle'=>null,'img'=>null], //หลักสูตรอบรม
            ['id' => 3,'name'=>'Exam','title'=>"ทดสอบหลักสูตร",'subtitle'=>null,'img'=>null], //ทดสอบหลักสูตร
            ['id' => 4,'name'=>'News_Blog','title'=>"ข่าวสาร / กิจกรรม",'subtitle'=>null,'img'=>null], //ข่าวสาร / กิจกรรม
            ['id' => 5,'name'=>'Gallary','title'=>"แกลลอรี่",'subtitle'=>null,'img'=>null], //เเกลลอรี่
            ['id' => 7,'name'=>'Cart','title'=>"ตะกร้าสินค้า",'subtitle'=>null,'img'=>null], //ตะกร้าสินค้า
            ['id' => 8,'name'=>'Pay','title'=>"ดำเนินการชำระเงิน",'subtitle'=>null,'img'=>null], //ดำเนินการชำระเงิน
            ['id' => 9,'name'=>'User','title'=>"ข้อมูลส่วนตัว",'subtitle'=>null,'img'=>null], //ข้อมูลส่วนตัว
            ['id' => 10,'name'=>'Help','title'=>"ช่วยเหลือ",'subtitle'=>null,'img'=>null], //ช่วยเหลือ,
        );
        foreach($array as $row){
            $Banner = new Banner;
            $Banner->banner_id = $row['id'];
            $Banner->banner_name = $row['name'];
            $Banner->banner_title = $row['title'];
            $Banner->banner_subtitle = $row['subtitle'];
            $Banner->banner_img = $row['img'];
            $Banner->save();
        }
    }
}
