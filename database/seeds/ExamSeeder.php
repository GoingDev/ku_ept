<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Exam;

class ExamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $array = array(
            [
                'ex_name' => 'KU-EPT 2562/4',
                'c_id' => 1,
                'ex_type' => 2,
                'ex_limit' => 50,
                'ex_people' => 100,
                'ex_price' => 2500,
                'ex_datestart' => '2018-02-05',
                'ex_dateend' => '2018-02-06',
                'ex_dateexam' => '2018-02-10',
                'ex_timeexam_start' => '09:00:00',
                'ex_timeexam_end' => '12:00:00',
                'ex_emailto_seat' => '2018-02-8',
                'ex_emailto_result' => '2018-02-20',
                'ex_status' => 1

            ],
            [
                'ex_name' => 'KU-EPT 2562/4',
                'c_id' => 1,
                'ex_type' => 2,
                'ex_limit' => 50,
                'ex_people' => 100,
                'ex_price' => 2500,
                'ex_datestart' => '2018-02-05',
                'ex_dateend' => '2018-02-06',
                'ex_dateexam' => '2018-02-10',
                'ex_timeexam_start' => '09:00:00',
                'ex_timeexam_end' => '12:00:00',
                'ex_emailto_seat' => '2018-02-8',
                'ex_emailto_result' => '2018-02-20',
                'ex_status' => 1

            ],
            [
                'ex_name' => 'KU-EPT 2562/3',
                'c_id' => 2,
                'ex_type' => 2,
                'ex_limit' => 30,
                'ex_people' => 400,
                'ex_price' => 4000,
                'ex_datestart' => '2018-02-15',
                'ex_dateend' => '2018-02-20',
                'ex_dateexam' => '2018-02-24',
                'ex_timeexam_start' => '09:00:00',
                'ex_timeexam_end' => '12:00:00',
                'ex_emailto_seat' => '2018-03-1',
                'ex_emailto_result' => '2018-03-20',
                'ex_status' => 0

            ],
            [
                'ex_name' => 'KU-EPT 2562/2',
                'c_id' => 3,
                'ex_type' => 3,
                'ex_limit' => 55,
                'ex_people' => 300,
                'ex_price' => 600,
                'ex_datestart' => '2018-01-05',
                'ex_dateend' => '2018-01-06',
                'ex_dateexam' => '2018-01-10',
                'ex_timeexam_start' => '09:00:00',
                'ex_timeexam_end' => '15:00:00',
                'ex_emailto_seat' => '2018-01-8',
                'ex_emailto_result' => '2018-01-20',
                'ex_status' => 1

            ],
            [
                'ex_name' => 'KU-EPT 2562/4',
                'c_id' => 3,
                'ex_type' => 2,
                'ex_limit' => 25,
                'ex_people' => 34,
                'ex_price' => 650,
                'ex_datestart' => '2018-02-06',
                'ex_dateend' => '2018-02-07',
                'ex_dateexam' => '2018-02-11',
                'ex_timeexam_start' => '09:00:00',
                'ex_timeexam_end' => '12:00:00',
                'ex_emailto_seat' => '2018-02-9',
                'ex_emailto_result' => '2018-02-21',
                'ex_status' => 0

            ],
            [
                'ex_name' => 'KU-EPT 2562/4',
                'c_id' => 4,
                'ex_type' => 0,
                'ex_limit' => 25,
                'ex_people' => 80,
                'ex_price' => 450,
                'ex_datestart' => '2018-03-06',
                'ex_dateend' => '2018-03-07',
                'ex_dateexam' => '2018-03-11',
                'ex_timeexam_start' => '15:00:00',
                'ex_timeexam_end' => '17:00:00',
                'ex_emailto_seat' => '2018-03-9',
                'ex_emailto_result' => '2018-04-21',
                'ex_status' => 0

            ],
            [
                'ex_name' => 'KU-EPT 2562/3',
                'c_id' => 3,
                'ex_type' => 2,
                'ex_limit' => 58,
                'ex_people' => 124,
                'ex_price' => 1000,
                'ex_datestart' => '2018-07-05',
                'ex_dateend' => '2018-07-06',
                'ex_dateexam' => '2018-07-10',
                'ex_timeexam_start' => '12:00:00',
                'ex_timeexam_end' => '15:00:00',
                'ex_emailto_seat' => '2018-07-8',
                'ex_emailto_result' => '2018-07-20',
                'ex_status' => 0

            ],
            [
                'ex_name' => 'KU-EPT 2562/2',
                'c_id' => 1,
                'ex_type' => 2,
                'ex_limit' => 30,
                'ex_people' => 30,
                'ex_price' => 900,
                'ex_datestart' => '2018-04-05',
                'ex_dateend' => '2018-04-06',
                'ex_dateexam' => '2018-04-10',
                'ex_timeexam_start' => '10:00:00',
                'ex_timeexam_end' => '14:00:00',
                'ex_emailto_seat' => '2018-04-08',
                'ex_emailto_result' => '2018-04-20',
                'ex_status' => 1

            ],
            [
                'ex_name' => 'KU-EPT 2562/4',
                'c_id' => 1,
                'ex_type' => 2,
                'ex_limit' => 50,
                'ex_people' => 100,
                'ex_price' => 2500,
                'ex_datestart' => '2018-02-05',
                'ex_dateend' => '2018-02-06',
                'ex_dateexam' => '2018-02-10',
                'ex_timeexam_start' => '09:00:00',
                'ex_timeexam_end' => '12:00:00',
                'ex_emailto_seat' => '2018-02-8',
                'ex_emailto_result' => '2018-02-20',
                'ex_status' => 1

            ],
            [
                'ex_name' => 'KU-EPT 2562/4',
                'c_id' => 1,
                'ex_type' => 2,
                'ex_limit' => 50,
                'ex_people' => 100,
                'ex_price' => 1100,
                'ex_datestart' => '2018-05-06',
                'ex_dateend' => '2018-05-07',
                'ex_dateexam' => '2018-05-11',
                'ex_timeexam_start' => '12:00:00',
                'ex_timeexam_end' => '15:00:00',
                'ex_emailto_seat' => '2018-05-9',
                'ex_emailto_result' => '2018-05-20',
                'ex_status' => 0

            ],
            [
                'ex_name' => 'KU-EPT 2562/0',
                'c_id' => 1,
                'ex_type' => 0,
                'ex_limit' => 150,
                'ex_people' => 300,
                'ex_price' => 2000,
                'ex_datestart' => '2018-02-05',
                'ex_dateend' => '2019-02-06',
                'ex_dateexam' => '2019-02-10',
                'ex_timeexam_start' => '09:00:00',
                'ex_timeexam_end' => '12:00:00',
                'ex_emailto_seat' => '2019-02-8',
                'ex_emailto_result' => '2019-02-20',
                'ex_status' => 0

            ],
            [
                'ex_name' => 'KU-EPT 2562/3',
                'c_id' => 1,
                'ex_type' => 2,
                'ex_limit' => 20,
                'ex_people' => 10,
                'ex_price' => 2000,
                'ex_datestart' => '2019-05-05',
                'ex_dateend' => '2019-05-06',
                'ex_dateexam' => '2019-05-10',
                'ex_timeexam_start' => '08:00:00',
                'ex_timeexam_end' => '11:00:00',
                'ex_emailto_seat' => '2019-05-8',
                'ex_emailto_result' => '2019-05-20',
                'ex_status' => 1

            ],
            [
                'ex_name' => 'KU-EPT 2562/12',
                'c_id' => 1,
                'ex_type' => 2,
                'ex_limit' => 50,
                'ex_people' => 100,
                'ex_price' => 3000,
                'ex_datestart' => '2018-11-05',
                'ex_dateend' => '2018-11-06',
                'ex_dateexam' => '2018-11-10',
                'ex_timeexam_start' => '09:00:00',
                'ex_timeexam_end' => '12:00:00',
                'ex_emailto_seat' => '2018-11-8',
                'ex_emailto_result' => '2018-11-20',
                'ex_status' => 0

            ],
            [
                'ex_name' => 'KU-EPT 2562/9',
                'c_id' => 1,
                'ex_type' => 1,
                'ex_limit' => 50,
                'ex_people' => 100,
                'ex_price' => 2500,
                'ex_datestart' => '2018-12-05',
                'ex_dateend' => '2018-12-06',
                'ex_dateexam' => '2018-12-10',
                'ex_timeexam_start' => '09:00:00',
                'ex_timeexam_end' => '12:00:00',
                'ex_emailto_seat' => '2018-12-8',
                'ex_emailto_result' => '2018-12-20',
                'ex_status' => 1

            ],
        );
        foreach($array as $row){
            Exam::create($row);
        }
    }
}
