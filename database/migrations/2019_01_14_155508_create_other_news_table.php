<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtherNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('other_news', function (Blueprint $table) {
            $table->increments('othernews_id');
            $table->integer('othernews_cid');
            $table->string('othernews_img')->nullable();
            $table->string('othernews_title')->nullable();
            $table->text('othernews_content')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('other_news');
    }
}
