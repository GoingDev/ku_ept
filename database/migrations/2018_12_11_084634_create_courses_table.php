<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('course_id');
            $table->string('course_name');
            $table->text('course_description')->nullable();
            $table->text('course_title')->nullable();
            $table->integer('course_unit')->default(0);
            $table->integer('course_price');
            $table->integer('course_people');
            $table->integer('course_status');
            $table->integer('c_id');
            $table->text('course_img')->nullable();
            $table->date('course_datestart');
            $table->date('course_dateend');
            $table->time('course_timestart');
            $table->time('course_timeend');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
