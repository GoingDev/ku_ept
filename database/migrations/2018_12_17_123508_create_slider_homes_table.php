<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSliderHomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slider_homes', function (Blueprint $table) {
            $table->increments('sli_id');
            $table->text('sli_img')->nullable();
            $table->string('sli_name')->nullable();
            $table->string('sli_title')->nullable();
            $table->string('sli_subtitle')->nullable();
            $table->string('sli_headtitle')->nullable();
            $table->string('sli_btn1_link')->nullable();
            $table->string('sli_btn1_text')->nullable();
            $table->string('sli_btn2_link')->nullable();
            $table->string('sli_btn2_text')->nullable();
            $table->integer('sli_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slider_homes');
    }
}
