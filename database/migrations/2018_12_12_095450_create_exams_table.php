<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exams', function (Blueprint $table) {
            $table->increments('ex_id');
            $table->string('ex_name');
            $table->integer('c_id');
            $table->integer('ex_type');
            $table->integer('ex_limit');
            $table->integer('ex_people')->nullable();
            $table->integer('ex_unit')->default(0);
            $table->integer('ex_price');
            $table->date('ex_datestart');
            $table->date('ex_dateend');
            $table->date('ex_dateexam');
            $table->time('ex_timeexam_start');
            $table->time('ex_timeexam_end');
            $table->date('ex_emailto_seat');
            $table->date('ex_emailto_result');
            $table->integer('ex_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exams');
    }
}
