<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('order_id');
            $table->integer('order_price');
            $table->integer('order_type');
            $table->string('order_code')->unique();
            $table->integer('order_discount')->default(0);
            $table->text('order_discount_detail')->nullable();
            $table->text('order_address')->nullable();
            $table->text('order_address_slip')->nullable();
            $table->integer('order_status');
            $table->integer('u_id')->nullable();
            $table->string('u_name')->nullable();
            $table->string('u_name_TH')->nullable();
            $table->string('u_tel')->nullable();
            $table->date('u_birthday')->nullable();
            $table->string('u_email')->nullable();
            $table->timestamps();
        });
    }
    //type 0 course type 1 exam
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
