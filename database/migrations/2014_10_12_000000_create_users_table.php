<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('name_TH');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('facebook_id')->unique()->nullable();
            $table->integer('status');
            $table->string('tel')->nullable();
            $table->text('address')->nullable();
            $table->date('birthday')->nullable();
            $table->string('ss_id')->nullable();
            $table->integer('province')->nullable();
            $table->integer('amphur')->nullable();
            $table->integer('district')->nullable();
            $table->string('line_id')->nullable();
            $table->tinyInteger('slip_type')->default(1);
            $table->integer('code')->nullable();
            $table->text('address_slip')->nullable();
            $table->integer('prov_slip')->nullable();
            $table->integer('amph_slip')->nullable();
            $table->integer('dist_slip')->nullable();
            $table->integer('code_slip')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
