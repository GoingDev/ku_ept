/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 426);
/******/ })
/************************************************************************/
/******/ ({

/***/ 426:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(427);


/***/ }),

/***/ 427:
/***/ (function(module, exports) {

jQuery(document).ready(function ($) {
  console.log('JS'); // remove Loader

  $("#main-loader").fadeOut("fast", function () {
    $('#main-loader').remove();
  }); // toggle Board

  var $listBoardItem = $('.hasTable');
  $toggleTable = $('.toggle-table');
  $listBoardItem.eq(0).addClass('active');
  $toggleTable.eq(0).addClass('active');
  $listBoardItem.click(function (e) {
    e.stopPropagation();
    $listBoardItem.removeClass('active');
    $toggleTable.removeClass('active');
    $(this).addClass('active');
    $toggleTable.eq($(this).index()).addClass('active');
  }); //tabhome

  var $homeTabItem = $('p.homeTab_item'),
      $wrapHomeTabContent = $('.wrap-homeTab-content');
  $wrapHomeTabContent.removeClass('active').eq(0).addClass('active');
  $homeTabItem.removeClass('active').eq(0).addClass('active');
  $homeTabItem.click(function (e) {
    e.stopPropagation();
    var $this = $(this),
        $thisData = $this.data().tab;
    $homeTabItem.removeClass('active');
    $wrapHomeTabContent.removeClass('active');
    $this.addClass('active');
    $wrapHomeTabContent.each(function (index, el) {
      if ($(this).data().tab == $thisData) {
        $(this).addClass('active');
        return false;
      }
    });
  }); // tab mini

  var $homeTabItemMini = $('.tabHome-mini'),
      $tabMiniTabHome = $('.tabHome-content-mini');
  $homeTabItemMini.removeClass('active').eq(0).addClass('active');
  $tabMiniTabHome.removeClass('active').eq(0).addClass('active');
  $homeTabItemMini.click(function (e) {
    e.stopPropagation();
    var $this = $(this),
        $thisData = $this.data().tab;
    $tabMiniTabHome.removeClass('active');
    $homeTabItemMini.removeClass('active');
    $this.addClass('active');
    $tabMiniTabHome.each(function (index, el) {
      if ($(this).data().tab == $thisData) {
        $(this).addClass('active');
        return false;
      }
    });
  }); // hover google

  var $cardContact = $('.card-contact');
  $('.map-g').hover(function (e) {
    $cardContact.addClass('fade');
  }, function (e) {
    $cardContact.removeClass('fade');
  }); // // slider Home
  // $('#sliderHome').slick({
  //     arrows: false,
  //     dots: true,
  //     appendDots: $('#renderDotsHome'),
  //     dotsClass: 'homeDots',
  //     pauseOnHover: true
  // });

  var $howPaymentToggle = $('.howPayment-toggle'),
      $howPaymentBodyToggle = $('.howPayment-body-toggle');
  $howPaymentToggle.removeClass('active').eq(0).addClass('active');
  $howPaymentBodyToggle.removeClass('active').eq(0).addClass('active');
  $howPaymentToggle.click(function (e) {
    e.stopPropagation();
    $howPaymentToggle.removeClass('active');
    $howPaymentBodyToggle.removeClass('active');
    $(this).addClass('active');
    $howPaymentBodyToggle.eq($(this).index()).addClass('active');
  });
  var $profileHistoryList = $('.profile-history-list'),
      $basketList = $('.basket-list');
  $profileHistoryList.click(function (e) {
    console.log($(this).next());
    $basketList.removeClass('active');
    $profileHistoryList.removeClass('active');
    $(this).addClass('active');
    $(this).next().addClass('active');
  });
});

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgMDQ1OTkxMWZhNDRkZTk0ZDZhM2EiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL3NjcmlwdC5qcyJdLCJuYW1lcyI6WyJqUXVlcnkiLCJkb2N1bWVudCIsInJlYWR5IiwiJCIsImNvbnNvbGUiLCJsb2ciLCJmYWRlT3V0IiwicmVtb3ZlIiwiJGxpc3RCb2FyZEl0ZW0iLCIkdG9nZ2xlVGFibGUiLCJlcSIsImFkZENsYXNzIiwiY2xpY2siLCJlIiwic3RvcFByb3BhZ2F0aW9uIiwicmVtb3ZlQ2xhc3MiLCJpbmRleCIsIiRob21lVGFiSXRlbSIsIiR3cmFwSG9tZVRhYkNvbnRlbnQiLCIkdGhpcyIsIiR0aGlzRGF0YSIsImRhdGEiLCJ0YWIiLCJlYWNoIiwiZWwiLCIkaG9tZVRhYkl0ZW1NaW5pIiwiJHRhYk1pbmlUYWJIb21lIiwiJGNhcmRDb250YWN0IiwiaG92ZXIiLCIkaG93UGF5bWVudFRvZ2dsZSIsIiRob3dQYXltZW50Qm9keVRvZ2dsZSIsIiRwcm9maWxlSGlzdG9yeUxpc3QiLCIkYmFza2V0TGlzdCIsIm5leHQiXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7OztBQzdEQUEsTUFBTSxDQUFDQyxRQUFELENBQU4sQ0FBaUJDLEtBQWpCLENBQXVCLFVBQVNDLENBQVQsRUFBWTtBQUMvQkMsU0FBTyxDQUFDQyxHQUFSLENBQVksSUFBWixFQUQrQixDQUsvQjs7QUFFQUYsR0FBQyxDQUFFLGNBQUYsQ0FBRCxDQUFvQkcsT0FBcEIsQ0FBNkIsTUFBN0IsRUFBcUMsWUFBVztBQUM1Q0gsS0FBQyxDQUFDLGNBQUQsQ0FBRCxDQUFrQkksTUFBbEI7QUFDRCxHQUZILEVBUCtCLENBWS9COztBQUNBLE1BQU1DLGNBQWMsR0FBR0wsQ0FBQyxDQUFDLFdBQUQsQ0FBeEI7QUFDUU0sY0FBWSxHQUFHTixDQUFDLENBQUMsZUFBRCxDQUFoQjtBQUNKSyxnQkFBYyxDQUFDRSxFQUFmLENBQWtCLENBQWxCLEVBQXFCQyxRQUFyQixDQUE4QixRQUE5QjtBQUNBRixjQUFZLENBQUNDLEVBQWIsQ0FBZ0IsQ0FBaEIsRUFBbUJDLFFBQW5CLENBQTRCLFFBQTVCO0FBQ0pILGdCQUFjLENBQUNJLEtBQWYsQ0FBcUIsVUFBU0MsQ0FBVCxFQUFXO0FBQzVCQSxLQUFDLENBQUNDLGVBQUY7QUFDQU4sa0JBQWMsQ0FBQ08sV0FBZixDQUEyQixRQUEzQjtBQUNBTixnQkFBWSxDQUFDTSxXQUFiLENBQXlCLFFBQXpCO0FBQ0FaLEtBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUVEsUUFBUixDQUFpQixRQUFqQjtBQUNBRixnQkFBWSxDQUFDQyxFQUFiLENBQWdCUCxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFhLEtBQVIsRUFBaEIsRUFBaUNMLFFBQWpDLENBQTBDLFFBQTFDO0FBQ0gsR0FORCxFQWpCK0IsQ0E0Qi9COztBQUNBLE1BQU1NLFlBQVksR0FBR2QsQ0FBQyxDQUFDLGdCQUFELENBQXRCO0FBQUEsTUFDUWUsbUJBQW1CLEdBQUdmLENBQUMsQ0FBQyx1QkFBRCxDQUQvQjtBQUVBZSxxQkFBbUIsQ0FBQ0gsV0FBcEIsQ0FBZ0MsUUFBaEMsRUFBMENMLEVBQTFDLENBQTZDLENBQTdDLEVBQWdEQyxRQUFoRCxDQUF5RCxRQUF6RDtBQUNBTSxjQUFZLENBQUNGLFdBQWIsQ0FBeUIsUUFBekIsRUFBbUNMLEVBQW5DLENBQXNDLENBQXRDLEVBQXlDQyxRQUF6QyxDQUFrRCxRQUFsRDtBQUVBTSxjQUFZLENBQUNMLEtBQWIsQ0FBbUIsVUFBU0MsQ0FBVCxFQUFXO0FBQzFCQSxLQUFDLENBQUNDLGVBQUY7QUFDQSxRQUFNSyxLQUFLLEdBQUdoQixDQUFDLENBQUMsSUFBRCxDQUFmO0FBQUEsUUFDUWlCLFNBQVMsR0FBR0QsS0FBSyxDQUFDRSxJQUFOLEdBQWFDLEdBRGpDO0FBRUFMLGdCQUFZLENBQUNGLFdBQWIsQ0FBeUIsUUFBekI7QUFDQUcsdUJBQW1CLENBQUNILFdBQXBCLENBQWdDLFFBQWhDO0FBQ0FJLFNBQUssQ0FBQ1IsUUFBTixDQUFlLFFBQWY7QUFDQU8sdUJBQW1CLENBQUNLLElBQXBCLENBQXlCLFVBQVNQLEtBQVQsRUFBZVEsRUFBZixFQUFrQjtBQUN2QyxVQUFHckIsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRa0IsSUFBUixHQUFlQyxHQUFmLElBQXNCRixTQUF6QixFQUFtQztBQUMvQmpCLFNBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUVEsUUFBUixDQUFpQixRQUFqQjtBQUNBLGVBQU8sS0FBUDtBQUNIO0FBQ0osS0FMRDtBQU9ILEdBZEQsRUFsQytCLENBbUQvQjs7QUFDQSxNQUFNYyxnQkFBZ0IsR0FBR3RCLENBQUMsQ0FBQyxlQUFELENBQTFCO0FBQUEsTUFDSXVCLGVBQWUsR0FBR3ZCLENBQUMsQ0FBQyx1QkFBRCxDQUR2QjtBQUVBc0Isa0JBQWdCLENBQUNWLFdBQWpCLENBQTZCLFFBQTdCLEVBQXVDTCxFQUF2QyxDQUEwQyxDQUExQyxFQUE2Q0MsUUFBN0MsQ0FBc0QsUUFBdEQ7QUFDQWUsaUJBQWUsQ0FBQ1gsV0FBaEIsQ0FBNEIsUUFBNUIsRUFBc0NMLEVBQXRDLENBQXlDLENBQXpDLEVBQTRDQyxRQUE1QyxDQUFxRCxRQUFyRDtBQUVBYyxrQkFBZ0IsQ0FBQ2IsS0FBakIsQ0FBdUIsVUFBU0MsQ0FBVCxFQUFXO0FBQzlCQSxLQUFDLENBQUNDLGVBQUY7QUFDQSxRQUFNSyxLQUFLLEdBQUdoQixDQUFDLENBQUMsSUFBRCxDQUFmO0FBQUEsUUFDSWlCLFNBQVMsR0FBR0QsS0FBSyxDQUFDRSxJQUFOLEdBQWFDLEdBRDdCO0FBR0FJLG1CQUFlLENBQUNYLFdBQWhCLENBQTRCLFFBQTVCO0FBQ0FVLG9CQUFnQixDQUFDVixXQUFqQixDQUE2QixRQUE3QjtBQUNBSSxTQUFLLENBQUNSLFFBQU4sQ0FBZSxRQUFmO0FBQ0FlLG1CQUFlLENBQUNILElBQWhCLENBQXFCLFVBQVNQLEtBQVQsRUFBZVEsRUFBZixFQUFrQjtBQUNuQyxVQUFHckIsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRa0IsSUFBUixHQUFlQyxHQUFmLElBQXNCRixTQUF6QixFQUFtQztBQUMvQmpCLFNBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUVEsUUFBUixDQUFpQixRQUFqQjtBQUNBLGVBQU8sS0FBUDtBQUNIO0FBQ0osS0FMRDtBQU1ILEdBZEQsRUF6RCtCLENBMkUvQjs7QUFDQSxNQUFNZ0IsWUFBWSxHQUFHeEIsQ0FBQyxDQUFDLGVBQUQsQ0FBdEI7QUFDQUEsR0FBQyxDQUFDLFFBQUQsQ0FBRCxDQUFZeUIsS0FBWixDQUFrQixVQUFTZixDQUFULEVBQVc7QUFDekJjLGdCQUFZLENBQUNoQixRQUFiLENBQXNCLE1BQXRCO0FBQ0gsR0FGRCxFQUVFLFVBQVNFLENBQVQsRUFBVztBQUNUYyxnQkFBWSxDQUFDWixXQUFiLENBQXlCLE1BQXpCO0FBQ0gsR0FKRCxFQTdFK0IsQ0F1Ri9CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7O0FBT0UsTUFBTWMsaUJBQWlCLEdBQUcxQixDQUFDLENBQUMsb0JBQUQsQ0FBM0I7QUFBQSxNQUNVMkIscUJBQXFCLEdBQUczQixDQUFDLENBQUMseUJBQUQsQ0FEbkM7QUFFTTBCLG1CQUFpQixDQUFDZCxXQUFsQixDQUE4QixRQUE5QixFQUF3Q0wsRUFBeEMsQ0FBMkMsQ0FBM0MsRUFBOENDLFFBQTlDLENBQXVELFFBQXZEO0FBQ0FtQix1QkFBcUIsQ0FBQ2YsV0FBdEIsQ0FBa0MsUUFBbEMsRUFBNENMLEVBQTVDLENBQStDLENBQS9DLEVBQWtEQyxRQUFsRCxDQUEyRCxRQUEzRDtBQUNBa0IsbUJBQWlCLENBQUNqQixLQUFsQixDQUF3QixVQUFTQyxDQUFULEVBQVc7QUFDL0JBLEtBQUMsQ0FBQ0MsZUFBRjtBQUNBZSxxQkFBaUIsQ0FBQ2QsV0FBbEIsQ0FBOEIsUUFBOUI7QUFDQWUseUJBQXFCLENBQUNmLFdBQXRCLENBQWtDLFFBQWxDO0FBQ0FaLEtBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUVEsUUFBUixDQUFpQixRQUFqQjtBQUNBbUIseUJBQXFCLENBQUNwQixFQUF0QixDQUF5QlAsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRYSxLQUFSLEVBQXpCLEVBQTBDTCxRQUExQyxDQUFtRCxRQUFuRDtBQUNILEdBTkQ7QUFTQSxNQUFNb0IsbUJBQW1CLEdBQUc1QixDQUFDLENBQUMsdUJBQUQsQ0FBN0I7QUFBQSxNQUNBNkIsV0FBVyxHQUFHN0IsQ0FBQyxDQUFDLGNBQUQsQ0FEZjtBQUVSNEIscUJBQW1CLENBQUNuQixLQUFwQixDQUEwQixVQUFTQyxDQUFULEVBQVc7QUFDakNULFdBQU8sQ0FBQ0MsR0FBUixDQUFZRixDQUFDLENBQUMsSUFBRCxDQUFELENBQVE4QixJQUFSLEVBQVo7QUFDQUQsZUFBVyxDQUFDakIsV0FBWixDQUF3QixRQUF4QjtBQUNBZ0IsdUJBQW1CLENBQUNoQixXQUFwQixDQUFnQyxRQUFoQztBQUNBWixLQUFDLENBQUMsSUFBRCxDQUFELENBQVFRLFFBQVIsQ0FBaUIsUUFBakI7QUFDQVIsS0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFROEIsSUFBUixHQUFldEIsUUFBZixDQUF3QixRQUF4QjtBQUNILEdBTkQ7QUFRSCxDQTdIRCxFIiwiZmlsZSI6Ii9qcy9zY3JpcHQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCIvXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gNDI2KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCAwNDU5OTExZmE0NGRlOTRkNmEzYSIsImpRdWVyeShkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oJCkge1xuICAgIGNvbnNvbGUubG9nKCdKUycpO1xuICAgXG5cblxuICAgIC8vIHJlbW92ZSBMb2FkZXJcbiAgICBcbiAgICAkKCBcIiNtYWluLWxvYWRlclwiICkuZmFkZU91dCggXCJmYXN0XCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICAkKCcjbWFpbi1sb2FkZXInKS5yZW1vdmUoKTtcbiAgICAgIH0pO1xuICAgICAgXG5cbiAgICAvLyB0b2dnbGUgQm9hcmRcbiAgICBjb25zdCAkbGlzdEJvYXJkSXRlbSA9ICQoJy5oYXNUYWJsZScpXG4gICAgICAgICAgICAkdG9nZ2xlVGFibGUgPSAkKCcudG9nZ2xlLXRhYmxlJyk7XG4gICAgICAgICRsaXN0Qm9hcmRJdGVtLmVxKDApLmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgJHRvZ2dsZVRhYmxlLmVxKDApLmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgICAkbGlzdEJvYXJkSXRlbS5jbGljayhmdW5jdGlvbihlKXtcbiAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgJGxpc3RCb2FyZEl0ZW0ucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAkdG9nZ2xlVGFibGUucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAkKHRoaXMpLmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgJHRvZ2dsZVRhYmxlLmVxKCQodGhpcykuaW5kZXgoKSkuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuICAgIH0pO1xuXG5cblxuXG4gICAgLy90YWJob21lXG4gICAgY29uc3QgJGhvbWVUYWJJdGVtID0gJCgncC5ob21lVGFiX2l0ZW0nKSxcbiAgICAgICAgICAgICR3cmFwSG9tZVRhYkNvbnRlbnQgPSAkKCcud3JhcC1ob21lVGFiLWNvbnRlbnQnKTtcbiAgICAkd3JhcEhvbWVUYWJDb250ZW50LnJlbW92ZUNsYXNzKCdhY3RpdmUnKS5lcSgwKS5hZGRDbGFzcygnYWN0aXZlJyk7XG4gICAgJGhvbWVUYWJJdGVtLnJlbW92ZUNsYXNzKCdhY3RpdmUnKS5lcSgwKS5hZGRDbGFzcygnYWN0aXZlJyk7XG4gICAgXG4gICAgJGhvbWVUYWJJdGVtLmNsaWNrKGZ1bmN0aW9uKGUpe1xuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICBjb25zdCAkdGhpcyA9ICQodGhpcyksXG4gICAgICAgICAgICAgICAgJHRoaXNEYXRhID0gJHRoaXMuZGF0YSgpLnRhYjtcbiAgICAgICAgJGhvbWVUYWJJdGVtLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgJHdyYXBIb21lVGFiQ29udGVudC5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICR0aGlzLmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgJHdyYXBIb21lVGFiQ29udGVudC5lYWNoKGZ1bmN0aW9uKGluZGV4LGVsKXtcbiAgICAgICAgICAgIGlmKCQodGhpcykuZGF0YSgpLnRhYiA9PSAkdGhpc0RhdGEpe1xuICAgICAgICAgICAgICAgICQodGhpcykuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICB9KTtcblxuXG4gICAgLy8gdGFiIG1pbmlcbiAgICBjb25zdCAkaG9tZVRhYkl0ZW1NaW5pID0gJCgnLnRhYkhvbWUtbWluaScpLFxuICAgICAgICAkdGFiTWluaVRhYkhvbWUgPSAkKCcudGFiSG9tZS1jb250ZW50LW1pbmknKTtcbiAgICAkaG9tZVRhYkl0ZW1NaW5pLnJlbW92ZUNsYXNzKCdhY3RpdmUnKS5lcSgwKS5hZGRDbGFzcygnYWN0aXZlJyk7XG4gICAgJHRhYk1pbmlUYWJIb21lLnJlbW92ZUNsYXNzKCdhY3RpdmUnKS5lcSgwKS5hZGRDbGFzcygnYWN0aXZlJyk7XG5cbiAgICAkaG9tZVRhYkl0ZW1NaW5pLmNsaWNrKGZ1bmN0aW9uKGUpe1xuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICBjb25zdCAkdGhpcyA9ICQodGhpcyksXG4gICAgICAgICAgICAkdGhpc0RhdGEgPSAkdGhpcy5kYXRhKCkudGFiO1xuICAgICAgICBcbiAgICAgICAgJHRhYk1pbmlUYWJIb21lLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgJGhvbWVUYWJJdGVtTWluaS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICR0aGlzLmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgJHRhYk1pbmlUYWJIb21lLmVhY2goZnVuY3Rpb24oaW5kZXgsZWwpe1xuICAgICAgICAgICAgaWYoJCh0aGlzKS5kYXRhKCkudGFiID09ICR0aGlzRGF0YSl7XG4gICAgICAgICAgICAgICAgJCh0aGlzKS5hZGRDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9KTtcblxuXG5cbiAgICAvLyBob3ZlciBnb29nbGVcbiAgICBjb25zdCAkY2FyZENvbnRhY3QgPSAkKCcuY2FyZC1jb250YWN0Jyk7XG4gICAgJCgnLm1hcC1nJykuaG92ZXIoZnVuY3Rpb24oZSl7XG4gICAgICAgICRjYXJkQ29udGFjdC5hZGRDbGFzcygnZmFkZScpO1xuICAgIH0sZnVuY3Rpb24oZSl7XG4gICAgICAgICRjYXJkQ29udGFjdC5yZW1vdmVDbGFzcygnZmFkZScpO1xuICAgIH0pO1xuXG5cbiAgIFxuXG5cbiAgICAvLyAvLyBzbGlkZXIgSG9tZVxuICAgIC8vICQoJyNzbGlkZXJIb21lJykuc2xpY2soe1xuICAgIC8vICAgICBhcnJvd3M6IGZhbHNlLFxuICAgIC8vICAgICBkb3RzOiB0cnVlLFxuICAgIC8vICAgICBhcHBlbmREb3RzOiAkKCcjcmVuZGVyRG90c0hvbWUnKSxcbiAgICAvLyAgICAgZG90c0NsYXNzOiAnaG9tZURvdHMnLFxuICAgIC8vICAgICBwYXVzZU9uSG92ZXI6IHRydWVcblxuICAgIC8vIH0pO1xuXG4gICBcblxuXG5cblxuICAgICAgY29uc3QgJGhvd1BheW1lbnRUb2dnbGUgPSAkKCcuaG93UGF5bWVudC10b2dnbGUnKSxcbiAgICAgICAgICAgICAgICAkaG93UGF5bWVudEJvZHlUb2dnbGUgPSAkKCcuaG93UGF5bWVudC1ib2R5LXRvZ2dsZScpO1xuICAgICAgICAgICAgJGhvd1BheW1lbnRUb2dnbGUucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpLmVxKDApLmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgICAgICRob3dQYXltZW50Qm9keVRvZ2dsZS5yZW1vdmVDbGFzcygnYWN0aXZlJykuZXEoMCkuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICAgJGhvd1BheW1lbnRUb2dnbGUuY2xpY2soZnVuY3Rpb24oZSl7XG4gICAgICAgICAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgICAgICAgICAkaG93UGF5bWVudFRvZ2dsZS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgICAgICAgJGhvd1BheW1lbnRCb2R5VG9nZ2xlLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgICAgICAgICAkKHRoaXMpLmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgICAgICAgICAkaG93UGF5bWVudEJvZHlUb2dnbGUuZXEoJCh0aGlzKS5pbmRleCgpKS5hZGRDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgICB9KTtcblxuXG4gICAgICAgICAgICBjb25zdCAkcHJvZmlsZUhpc3RvcnlMaXN0ID0gJCgnLnByb2ZpbGUtaGlzdG9yeS1saXN0JyksXG4gICAgICAgICAgICAkYmFza2V0TGlzdCA9ICQoJy5iYXNrZXQtbGlzdCcpO1xuICAgICRwcm9maWxlSGlzdG9yeUxpc3QuY2xpY2soZnVuY3Rpb24oZSl7XG4gICAgICAgIGNvbnNvbGUubG9nKCQodGhpcykubmV4dCgpKTtcbiAgICAgICAgJGJhc2tldExpc3QucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAkcHJvZmlsZUhpc3RvcnlMaXN0LnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgJCh0aGlzKS5hZGRDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICQodGhpcykubmV4dCgpLmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgICB9KTtcblxufSk7XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2pzL3NjcmlwdC5qcyJdLCJzb3VyY2VSb290IjoiIn0=