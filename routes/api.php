<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'api', 'as' => 'api.'], function () {
    route::get('getAllCategory','ProductController@getAllCategory');
    route::post('AddNewCate','ProductController@AddNewCate');
    route::get('getAllCourse','ProductController@getAllCourse');
    route::post('AddNewCourse','ProductController@AddNewCourse');
    route::post('EditCourse','ProductController@EditCourse');
    route::delete('DeleteCourse/{id}','ProductController@DeleteCourse');
    route::get('getAllExam','ProductController@getAllExam');
    route::post('AddNewExam','ProductController@AddNewExam');
    route::post('EditExam','ProductController@EditExam');
    route::delete('DeleteExam/{id}','ProductController@DeleteExam');
    route::get('getAllPromotion','ProductController@getAllPromotion');
    route::post('AddNewPromotion','ProductController@AddNewPromotion');
    route::delete('DeletePromotion/{id}','ProductController@DeletePromotion');
    route::post('ChangeStatusPromotion','ProductController@ChangeStatusPromotion');
    route::get('getAllFaq','ContentController@getAllFaq');
    route::post('AddNewFaq','ContentController@AddNewFaq');
    route::delete('DeleteFaq/{id}','ContentController@DeleteFaq');
    route::post('EditFaq','ContentController@EditFaq');
    route::post('EditRegis','ContentController@EditRegis');
    route::post('EditContent','ContentController@EditContent');
    route::get('getAllContent','ContentController@getAllContent');
    route::get('getAllNews','ContentController@getAllNews');
    route::post('AddNewBlog','ContentController@AddNewBlog');
    route::delete('DeleteBlog/{id}','ContentController@DeleteBlog');
    route::post('EditBlog','ContentController@EditBlog');
    route::post('AddNewGallary','ContentController@AddNewGallary');
    route::get('getAllGallary','ContentController@getAllGallary');
    route::get('getAllGallaryDetail','ContentController@getAllGallaryDetail');
    route::delete('DeleteGallary/{id}','ContentController@DeleteGallary');
    route::post('EditGallary','ContentController@EditGallary');
    route::post('AddNewSlider','ContentController@AddNewSlider');
    route::get('getAllSlider','ContentController@getAllSlider');
    route::delete('DeleteSlider/{id}','ContentController@DeleteSlider');
    route::post('EditSlider','ContentController@EditSlider');
    route::post('UpdateIndex','ContentController@UpdateIndex');
    route::get('getAllBanner','ContentController@getAllBanner');
    route::post('EditBanner','ContentController@EditBanner');
    route::post('findUser/{id}','UserController@findUser');
    route::post('sendMail','MailController@sendMail');
    route::get('getAllCart','ProductController@getAllCart');
    route::post('AddtoCart','ProductController@AddtoCart');
    route::delete('RemoveCart/{id}','ProductController@RemoveCart');
    route::get('getAllCode','ProductController@getAllCode');
    route::post('EditProfile','ContentController@EditProfile');
    route::get('province',function(){
        return DB::table('province')->get();
    });
    route::get('district',function(){
        return DB::table('district')->get();
    });
    route::get('amphur',function(){
        return DB::table('amphur')->get();
    });
    route::get('getAllOtherNews','ContentController@getAllOtherNews');
    route::post('AddNewOtherNews','ContentController@AddNewOtherNews');
    route::post('EditOtherNews','ContentController@EditOtherNews');
    route::delete('DeleteOtherNews/{id}','ContentController@DeleteOtherNews');
    route::get('getAllOrder','ProductController@getAllOrder');
    route::get('getAllOrderDetail','ProductController@getAllOrderDetail');
    route::get('getAllMember','ProductController@getAllMember');
    route::get('getOrderForView','ProductController@getOrderForView');
    route::post('changePassword','ContentController@changePassword');
});


    Route::post('auth/login', 'AuthController@login');
    Route::post('auth/checkFB','AuthController@checkFB');
    Route::post('auth/register', 'AuthController@register');

Route::group(['middleware' => ['jwt.auth']], function(){
  Route::get('auth/user', 'AuthController@user');
  Route::post('auth/logout', 'AuthController@logout');
});

Route::group(['middleware' => ['jwt.refresh']], function(){
  Route::get('auth/refresh', 'AuthController@refresh');
});
