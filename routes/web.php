<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->group(function () {
    Route::get('/','HomeController@index');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/Course/{id}', 'HomeController@index');
    Route::get('/NewCourse','HomeController@index');
    Route::get('/EditCourse/{id}','HomeController@index');
    Route::get('/ViewCourse/{id}','HomeController@index');
    Route::get('/Exam/{id}', 'HomeController@index');
    Route::get('/NewExam','HomeController@index');
    Route::get('/EditExam/{id}','HomeController@index');
    Route::get('/ViewExam/{id}','HomeController@index');
    Route::get('/ExamContent', 'HomeController@index');
    Route::get('/NewExamContent','HomeController@index');
    Route::get('/EditExamContent/{id}','HomeController@index');
    Route::get('/Help/{id}', 'HomeController@index');
    Route::get('/PageManage/{tag}', 'HomeController@index');
    Route::get('/Promotion', 'HomeController@index')->name('Promotion');
    Route::get('/Order', 'HomeController@index')->name('Order');
    Route::get('/Member', 'HomeController@index')->name('Member');
    Route::get('/MemberDetail/{id}', 'HomeController@index');
    Route::get('/PageManage/EditGallary/{id}','HomeController@index');
    Route::get('/PageManage/EditBlog/{id}','HomeController@index');
    Route::get('/PageManage/Home/NewSlider','HomeController@index');
    Route::get('/PageManage/Home/EditSlider/{id}','HomeController@index');
    Route::get('/orderCourse','HomeController@index');
    Route::get('/orderExam','HomeController@index');
    Route::get('/OrderDetail/{id}','HomeController@index');
});
Auth::routes();


Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/', 'UserController@index');
Route::get('/Home', 'UserController@index');
Route::get('/About', 'UserController@index');
Route::get('/Course/{id}', 'UserController@index');
Route::get('/CourseDetail/{id}', 'UserController@index');
Route::get('/Faq/{id}', 'UserController@index');
Route::get('/Gallery', 'UserController@index');
Route::get('/GallaryDetail/{id}', 'UserController@index');
Route::get('/Help', 'UserController@index');
Route::get('/News', 'UserController@index');
Route::get('/NewsDetail/{id}', 'UserController@index');
Route::get('/Contact', 'UserController@index');
Route::get('/Register', 'UserController@index');
Route::get('/Login', 'UserController@index');
Route::get('/Exam', 'UserController@index');

Route::get('/Basket', 'UserController@redirect');
Route::get('/DataUser', 'UserController@redirect');
Route::get('/DataUserExam', 'UserController@redirect');
Route::get('/HowPayment', 'UserController@redirect');
Route::get('/HowPaymentExam', 'UserController@redirect');

Route::get('/Profile', 'UserController@index');
Route::get('/EditProfile', 'UserController@index');
Route::get('/resetPassword', 'UserController@index');
Route::get('/Search', 'UserController@index');
Route::get('/examContent/{id}','UserController@index');
Route::post('/payCounterService','PaymentController@payCounterService');
Route::post('/payment_3d','PaymentController@payment_3d');
Route::post('/payPlus','PaymentController@payPlus');
Route::get('/home', 'HomeController@index')->name('home');
