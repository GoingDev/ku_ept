import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import _ from 'lodash'

Vue.use(Vuex)

const moduleProduct = {
    state: {
        course: [],
        category: [],
        exam: [],
        promotion: [],
        allcart: [],
        examselected: null,
        allorder: [],
        orderDetail: [],
        orderForView: []
    },
    getters: {
        getCourseById: (state) => (id) => {
            return state.course.filter(q => q.c_id == id)
        },
        getEdit_Course: (state) => (id) => {
            return state.course.find(q => q.course_id == id)
        },
        getCategory: (state) => (id) => {
            return state.category.find(q => q.c_id == id)
        },
        getCateExam: (state) => (id) => {
            return state.exam.filter(q => q.ex_type == id)
        },
        getEdit_Exam: (state) => (id) => {
            return state.exam.find(q => q.ex_id == id)
        },
        getCartByID: (state) => (id) => {
            return state.allcart.filter((q) => q.user_id == id);
        },
        getCartBySSID: (state) => (ssid) => {
            return state.allcart.filter((q) => q.ss_id == ssid);
        },
        findProCode: (state) => (code) => {
            return state.promotion.find((q) => q.pro_code == code);
        },
        orderCourse(state) {
            return state.allorder.filter((q) => q.order_type == 2);
        },
        orderExam(state) {
            return state.allorder.filter((q) => q.order_type == 1);
        },
        orderByUser: (state) => (u_id) => {
            return state.allorder.filter((q) => q.u_id == u_id);
        },
        orderDetailByCourseID: (state) => (id) => {
            return state.orderForView.filter((q) => q.course_id == id);
        },
        orderDetailByExamID: (state) => (id) => {
            return state.orderForView.filter((q) => q.exam_id == id);
        },
        orderByID: (state) => (id) => {
            return state.orderForView.find((q) => q.order_id == id);
        }
    },
    mutations: {
        selectExam(state, data) {
            state.examselected = data;
        },
        setCategory(state, data) {
            state.category = data;
        },
        setCourse(state, data) {
            state.course = data;
        },
        setExam(state, data) {
            state.exam = data;
        },
        setPromotion(state, data) {
            state.promotion = data;
        },
        setAllCart(state, data) {
            state.allcart = data;
        },
        setOrder(state, data) {
            state.allorder = data;
        },
        setOrderDetail(state, data) {
            state.orderDetail = data;
        },
        setOrderForView(state, data) {
            state.orderForView = data;
        }
    },
    actions: {
        getAllCategory({
            commit
        }) {
            axios.get('getAllCategory').then((res) => {
                commit('setCategory', res.data);
            });
        },
        getAllCourse({
            commit
        }) {
            axios.get('getAllCourse').then((res) => {
                commit('setCourse', res.data);
            });
        },
        getAllExam({
            commit
        }) {
            axios.get('getAllExam').then((res) => {
                commit('setExam', res.data);
            });
        },
        getAllPromotion({
            commit
        }) {
            axios.get('getAllPromotion').then((res) => {
                commit('setPromotion', res.data);
            });
        },
        getAllCart({
            commit
        }) {
            axios.get('getAllCart').then((res) => {
                commit('setAllCart', res.data);
            })
        },
        getAllOrder({
            commit
        }) {
            axios.get('/getAllOrder').then((res) => {
                commit('setOrder', res.data);
            });
        },
        getAllOrderDetail({
            commit
        }) {
            axios.get('/getAllOrderDetail').then((res) => {
                commit('setOrderDetail', res.data);
            });
        },
        getOrderForView({
            commit
        }) {
            axios.get('/getOrderForView').then((res) => {
                commit('setOrderForView', res.data);
            });
        }
    }
}
const moduleContent = {
    state: {
        faq: [],
        content: [],
        news: [],
        gallary: [],
        gallaryLoded: false,
        gallaryDetail: [],
        slider: [],
        banner: [],
        othernews: []
    },
    getters: {
        getSliderHome(state) {
            return state.slider.filter((v) => v.sli_status == 1);
        },
        getContentRegis(state) {
            return state.content.find((q) => q.ct_id == 1);
        },
        KueptHelp(state) {
            return state.content.filter((q) => q.ct_id > 1 && q.ct_id <= 5);
        },
        KuexiteHelp(state) {
            return state.content.filter((q) => q.ct_id > 5 && q.ct_id <= 9);
        },
        toeflitpHelp(state) {
            return state.content.filter((q) => q.ct_id == 10);
        },
        getContentAbout(state) {
            return state.content.find((q) => q.ct_id == 11);
        },
        BlogById: (state) => (id) => {
            let result = state.news.find((q) => q.nt_id == id);
            return result;
        },
        gallaryById: (state) => (id) => {
            return state.gallary.find((q) => q.g_id == id);
        },
        gallaryDetailById: (state) => (id) => {
            return state.gallaryDetail.filter((q) => q.g_id == id);
        },
        getContentContact(state) {
            return state.content.find((q) => q.ct_id == 12);
        },
        SliderById: (state) => (id) => {
            return state.slider.find((q) => q.sli_id == id);
        },
        newsPublic(state) {
            return state.news.filter((q) => q.nt_status == 1);
        },
        gallarayPublic(state) {
            return state.gallary.filter((q) => q.g_status == 1);
        },
        getContentById: (state) => (id) => {
            return state.content.find((q) => q.ct_id == id);
        },
        getBannerByID: (state) => (id) => {
            return state.banner.find((q) => q.banner_id == id);
        },
        othernewsByCID: (state) => (c_id) => {
            return state.othernews.filter((q) => q.othernews_cid == c_id);
        },
        otherNewsByID: (state) => (id) => {
            return state.othernews.find((q) => q.othernews_id == id);
        }
    },
    mutations: {
        setFaq(state, data) {
            state.faq = data;
        },
        setContent(state, data) {
            state.content = data;
        },
        setNews(state, data) {
            state.news = data;
        },
        setGallary(state, data) {
            state.gallary = data;
            state.gallaryLoded = true;
        },
        setGallaryDetail(state, data) {
            state.gallaryDetail = data
        },
        setAllSlider(state, data) {
            state.slider = data
        },
        setAllBanner(state, data) {
            state.banner = data;
        },
        setAllOtherNews(state, data) {
            state.othernews = data;
        },
        updateList(state, data) {
            state.slider = data
            axios.post('UpdateIndex', {
                    form: JSON.stringify(data)
                })
                .then((res) => {

                })
        }
    },
    actions: {
        getAllFaq({
            commit
        }) {
            axios.get('getAllFaq').then((res) => {
                commit('setFaq', res.data);
            });
        },
        getAllContent({
            commit
        }) {
            axios.get('getAllContent').then((res) => {
                commit('setContent', res.data)
            });
        },
        getAllNews({
            commit
        }) {
            axios.get('getAllNews').then((res) => {
                commit('setNews', res.data)
            });
        },
        getAllGallary({
            commit
        }) {
            axios.get('getAllGallary').then((res) => {
                commit('setGallary', res.data)
            });
        },
        getAllGallaryDetail({
            commit
        }) {
            axios.get('getAllGallaryDetail').then((res) => {
                commit('setGallaryDetail', res.data)
            });
        },
        getAllSlider({
            commit
        }) {
            axios.get('getAllSlider').then((res) => {
                console.log(res.data)
                commit('setAllSlider', res.data)
            });
        },
        getAllBanner({
            commit
        }) {
            axios.get('getAllBanner').then((res) => {
                commit('setAllBanner', res.data)
            });
        },
        getAllOtherNews({
            commit
        }) {
            axios.get('getAllOtherNews').then((res) => {
                commit('setAllOtherNews', res.data)
            });
        }
    }
}
const moduleAddress = {
    state: {
        amphur: [],
        district: [],
        province: [],
    },
    mutations: {
        setAmphur(state, data) {
            state.amphur = data
        },
        setDistrict(state, data) {
            state.district = data
        },
        setProvince(state, data) {
            state.province = data
        }
    },
    actions: {
        getAllAddress({
            commit
        }) {
            axios.get('province').then((res) => {
                commit('setProvince', res.data);
            });
            axios.get('district').then((res) => {
                commit('setDistrict', res.data);
            });
            axios.get('amphur').then((res) => {
                commit('setAmphur', res.data);
            });
        }
    }
}
const moduleUser = {
    state: {
        user: {
            id: null,
            address: null,
            amphur: null,
            birthday: null,
            code: null,
            district: null,
            email: null,
            name: null,
            name_TH : null,
            province: null,
            tel: null,
            status : 2,
            slip_type : true,
            address_slip : null,
            prov_slip : null,
            amph_slip : null,
            dist_slip : null,
            code_slip : null,
        },
        member: []
    },
    getters: {
        userByID: (state) => (id) => {
            return state.member.find((q) => q.id == id);
        }
    },
    mutations: {
        setUser(state, data) {
            state.user = data;
        },
        logout(state) {
            state.user = {
                id: null,
                address: null,
                amphur: null,
                birthday: null,
                code: null,
                district: null,
                email: null,
                name: null,
                name_TH : null,
                province: null,
                tel: null,
                status : 2,
                slip_type : true,
                address_slip : null,
                prov_slip : null,
                amph_slip : null,
                dist_slip : null,
                code_slip : null,
            };
        },
        setMember(state, data) {
            state.member = data;
        }
    },
    actions: {
        getAllMember({
            commit
        }) {
            axios.get('/getAllMember').then((res) => {
                commit('setMember', res.data);
            })
        }
    }
}
export default new Vuex.Store({
    modules: {
        moduleProduct: moduleProduct,
        moduleContent: moduleContent,
        moduleAddress: moduleAddress,
        moduleUser: moduleUser
    }
})
