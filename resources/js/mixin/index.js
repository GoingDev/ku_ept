export default {
    methods: {
        formatDate(date) {
            const realDate = date.split(' ');
            const months = ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ย.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.",
                "พ.ย.", "ธ.ค."
            ];
            let d = new Date(_.take(realDate));
            let day = d.getDate();
            let month = d.getMonth();
            let year = d.getFullYear();
            return day + " " + months[month] + " " + (year + 543);
        },
        formatDateTo(start, end) {
            const months = ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ย.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.",
                "พ.ย.", "ธ.ค."
            ];
            let d1 = new Date(start);
            let day1 = d1.getDate();
            let month1 = d1.getMonth();
            let year1 = d1.getFullYear();

            let d2 = new Date(end);
            let day2 = d2.getDate();
            let month2 = d2.getMonth();
            let year2 = d2.getFullYear();

            if (year1 == year2) {
                if (month1 == month2) {
                    return day1 + "-" + day2 + " " + months[month1] + " " + (parseInt(year1) + 543);
                } else {
                    return day1 + " " + months[month1] + " - " + day2 + " " + months[month2] + " " +
                        (parseInt(year1) + 543);
                }
            } else {
                return day1 + " " + months[month1] + " " + (parseInt(year1) + 543) + " - " +
                    day2 + " " + months[month2] + " " + (parseInt(year2) + 543);
            }

        }
    },
}