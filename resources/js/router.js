import Vue from 'vue'
import Router from 'vue-router'
import _ from 'lodash'
import HomePage from "./admin/views/Home.vue"
import CoursePage from "./admin/views/Course/Course.vue"
import ExamPage from "./admin/views/Exam/Exam.vue"
import NewExam from "./admin/views/Exam/NewExam.vue"
import EditExam from "./admin/views/Exam/EditExam.vue"
import ViewExam from "./admin/views/Exam/ViewExam.vue"
import NewCourse from "./admin/views/Course/NewCourse.vue"
import EditCourse from "./admin/views/Course/EditCourse.vue"
import ViewCourse from "./admin/views/Course/ViewCourse.vue"
import PromotionPage from "./admin/views/Promotion.vue"
import MemberPage from "./admin/views/Member/Member.vue"
import memberDetail from "./admin/views/Member/memberDetail.vue"
import HomePageManage from "./admin/views/PageManage/Home/HomePageManage.vue"
import newHome from "./admin/views/PageManage/Home/newHome.vue"
import EditHome from "./admin/views/PageManage/Home/EditHome.vue"
import AboutPageManage from "./admin/views/PageManage/AboutPageManage.vue"
import ContactPageManage from "./admin/views/PageManage/ContactPageManage.vue"
import GallaryPageManage from "./admin/views/PageManage/Gallary/GallaryPageManage.vue"
import NewsPageManage from "./admin/views/PageManage/News/NewsPageManage.vue"
import BannerManage from "./admin/views/PageManage/BannerManage.vue"
import faqHelp from "./admin/views/Help/faqHelp.vue"
import kueptHelp from "./admin/views/Help/kueptHelp.vue"
import kuexiteHelp from "./admin/views/Help/kuexiteHelp.vue"
import regisHelp from "./admin/views/Help/regisHelp.vue"
import toeflitpHelp from "./admin/views/Help/toeflitpHelp.vue"
import NewBlog from "./admin/views/PageManage/News/NewBlog.vue"
import EditBlog from "./admin/views/PageManage/News/EditBlog.vue"
import NewGallary from "./admin/views/PageManage/Gallary/NewGallary.vue"
import EditGallary from "./admin/views/PageManage/Gallary/EditGallary.vue"
import OtherNews from "./admin/views/OtherNews/OtherNews.vue"
import NewOtherNews from "./admin/views/OtherNews/newOther"
import EditOtherNews from "./admin/views/OtherNews/editOther"
import orderCourse from "./admin/views/Order/Course/orderCourse.vue"
import orderExam from "./admin/views/Order/Exam/orderExam.vue"
import OrderDetail from "./admin/views/OrderDetail.vue"

import Home from "./user/pages/home.vue"
import About from "./user/pages/about.vue"
import Contact from "./user/pages/contact.vue"
import CourseDetail from "./user/pages/courseDetail.vue"
import Course from "./user/pages/course.vue"
import Faq from "./user/pages/faq.vue"
import GalleryDetail from "./user/pages/galleryDetail.vue"
import Gallery from "./user/pages/gallery.vue"
import Help from "./user/pages/help.vue"
import NewsDetail from "./user/pages/newDetail.vue"
import News from "./user/pages/news.vue"
import Register from "./user/pages/register.vue"
import Login from "./user/pages/login.vue"
import Basket from "./user/pages/basket.vue"
import DataUser from "./user/pages/userpayment.vue"
import DataUserUnknow from "./user/pages/userpaymentUnknow.vue"
import HowPayment from "./user/pages/howPayment.vue"
import HowPaymentUnknow from "./user/pages/howPaymentUnknow.vue"
import Profile from "./user/pages/userprofile.vue"
import Exam from "./user/pages/exam.vue"
import EditProfile from "./user/pages/editProfile.vue"
import examContent from "./user/pages/examContent.vue"
import search from "./user/pages/search.vue"
import resetPassword from "./user/pages/resetPassword.vue"

Vue.use(_);
Vue.use(Router)

const RouterAdmin = [{
        name: "/admin/home",
        path: "/admin/home",
        component: HomePage
    },
    {
        name: "/admin/",
        path: "/admin/",
        component: HomePage
    },
    {
        name: "/admin/Course",
        path: "/admin/Course/:id",
        component: CoursePage,
        props: {
            id: 0
        }
    },
    {
        name: "/admin/Exam",
        path: "/admin/Exam/:id",
        component: ExamPage,
        props: {
            id: 0
        }
    },
    {
        path: "/admin/NewExam",
        component: NewExam
    },
    {
        name: "/admin/EditExam",
        path: "/admin/EditExam/:id",
        component: EditExam,
        props: {
            id: 0
        }
    },
    {
        name: "/admin/ViewExam",
        path: "/admin/ViewExam/:id",
        component: ViewExam,
        props: {
            id: 0
        }
    },
    {
        path: "/admin/NewCourse",
        component: NewCourse
    },
    {
        name: "/admin/EditCourse",
        path: "/admin/EditCourse/:id",
        component: EditCourse,
        props: {
            id: 0
        }
    },
    {
        name: "/admin/ViewCourse",
        path: "/admin/ViewCourse/:id",
        component: ViewCourse,
        props: {
            id: 0
        }
    },
    {
        path: "/admin/Promotion/",
        component: PromotionPage,
    },
    {
        path: "/admin/Member/",
        component: MemberPage,
    },
    {
        name: "/admin/MemberDetail",
        path: "/admin/MemberDetail/:id",
        component: memberDetail,
    },
    {
        path: "/admin/PageManage/News/",
        component: NewsPageManage,
    },
    {
        path: "/admin/PageManage/Home",
        component: HomePageManage,
    },
    {
        path: "/admin/PageManage/About",
        component: AboutPageManage,
    },
    {
        path: "/admin/PageManage/Contact",
        component: ContactPageManage,
    },
    {
        path: "/admin/PageManage/Banner",
        component: BannerManage,
    },
    {
        path: "/admin/PageManage/Gallary",
        component: GallaryPageManage,
    },
    {
        path: "/admin/Help/ปัญหาที่พบบ่อย_FAQ",
        component: faqHelp,
    },
    {
        path: "/admin/Help/การสอบ_KU_EPT",
        component: kueptHelp,
    },
    {
        path: "/admin/Help/การสอบ_KU_EXITE",
        component: kuexiteHelp,
    },
    {
        path: "/admin/Help/ขั้นตอนการสมัคร",
        component: regisHelp,
    },
    {
        path: "/admin/Help/การสอบ_TOEFL_ITP",
        component: toeflitpHelp,
    },
    {
        path: "/admin/PageManage/NewBlog",
        component: NewBlog
    },
    {
        name: "/admin/PageManage/EditBlog",
        path: "/admin/PageManage/EditBlog/:id",
        component: EditBlog,
        props: {
            id: 0
        }
    },
    {
        path: "/admin/PageManage/NewGallary",
        component: NewGallary
    },
    {
        name: "/admin/PageManage/EditGallary",
        path: "/admin/PageManage/EditGallary/:id",
        component: EditGallary,
        props: {
            id: 0
        }
    },
    {
        path: "/admin/PageManage/Home/NewSlider",
        component: newHome
    },
    {
        name: "/admin/PageManage/Home/EditSlider",
        path: "/admin/PageManage/Home/EditSlider/:id",
        component: EditHome
    },
    {
        path: "/admin/ExamContent",
        component: OtherNews
    },
    {
        path: "/admin/NewExamContent",
        component: NewOtherNews
    },
    {
        name: "/admin/EditExamContent",
        path: "/admin/EditExamContent/:id",
        component: EditOtherNews,
        props: {
            id: 0
        }
    },
    {
        path: "/admin/orderCourse",
        component: orderCourse
    },
    {
        path: "/admin/orderExam",
        component: orderExam
    },
    {
        name : "/admin/OrderDetail",
        path: "/admin/OrderDetail/:id",
        component: OrderDetail,
        props : {
            id : 0
        }
    },
]


const RouterUser = [{
        path: "/About",
        component: About,
        meta: {
            breadcrumb: [{
                    name: '<i class="fas fa-home"></i>',
                    link: '/Home'
                },
                {
                    name: 'เกี่ยวกับ',
                    link: '/About'
                }
            ]
        }
    },
    {
        path: "/Home",
        component: Home,
    },
    {
        path: "/",
        component: Home
    },
    {
        path: "/Contact",
        component: Contact,
        meta: {
            breadcrumb: [{
                    name: '<i class="fas fa-home"></i>',
                    link: '/Home'
                },
                {
                    name: 'ติดต่อ',
                    link: '/Contact'
                }
            ]
        }
    },
    {
        name: "/Course/",
        path: "/Course/:id",
        component: Course,
        props: {
            id: 0
        },
        meta: {
            breadcrumb: [{
                    name: '<i class="fas fa-home"></i>',
                    link: '/Home'
                },
                {
                    name: 'คอร์ส',
                    link: '/Course'
                }
            ]
        },

    },
    {
        name: "/CourseDetail/",
        path: "/CourseDetail/:id",
        component: CourseDetail,
        props: {
            id: 0
        },
        breadcrumb: [{
                name: '<i class="fas fa-home"></i>',
                link: '/Home'
            },
            {
                name: 'คอร์ส',
                link: '/Course'
            },
            {
                name: 'คอร์สรายละเอียด',
                link: '/CourseDetail'
            }
        ]
    },
    {
        name: "/Faq/",
        path: "/Faq/:id",
        component: Faq,
        breadcrumb: [{
                name: '<i class="fas fa-home"></i>',
                link: '/Home'
            },
            {
                name: 'คำถามที่พบบ่อย (FAQ)',
                link: '/Faq'
            },
        ]
    },
    {
        path: "/Exam",
        component: Exam,
        breadcrumb: [{
                name: '<i class="fas fa-home"></i>',
                link: '/Home'
            },
            {
                name: 'ทดสอบหลักสูตร',
                link: '/Exam'
            },
        ]
    },
    {
        name: "/GallaryDetail/",
        path: "/GallaryDetail/:id",
        component: GalleryDetail,
        props: {
            id: 0
        },
        meta: {
            breadcrumb: [{
                    name: '<i class="fas fa-home"></i>',
                    link: '/Home'
                },
                {
                    name: 'แกลอรี่ทั้งหมด',
                    link: '/Gallery'
                },
                {
                    name: 'แกลอรี่',
                    link: '/GallaryDetail'
                }
            ]
        }
    },
    {
        path: "/Gallery",
        component: Gallery,
        meta: {
            breadcrumb: [{
                    name: '<i class="fas fa-home"></i>',
                    link: '/Home'
                },
                {
                    name: 'แกลอรี่',
                    link: '/Gallery'
                }
            ]
        }
    },
    {
        path: "/Help",
        component: Help
    },
    {
        name: "/NewsDetail/",
        path: "/NewsDetail/:id",
        component: NewsDetail,
        props: {
            id: 0
        }
    },
    {
        path: "/News",
        component: News,
        meta: {
            breadcrumb: [{
                    name: '<i class="fas fa-home"></i>',
                    link: '/Home'
                },
                {
                    name: 'ข่าวและกิจกรรม',
                    link: '/News'
                }
            ]
        }
    },
    {
        path: "/Register",
        component: Register,
        meta: {
            breadcrumb: [{
                    name: '<i class="fas fa-home"></i>',
                    link: '/Home'
                },
                {
                    name: 'ลงทะเบียน',
                    link: '/Register'
                }
            ]
        }
    },
    {
        path: "/Login",
        component: Login,
        meta: {
            breadcrumb: [{
                    name: '<i class="fas fa-home"></i>',
                    link: '/Home'
                },
                {
                    name: 'เข้าสู่ระบบ',
                    link: '/Login'
                }
            ]
        }
    },
    {
        path: "/Basket",
        component: Basket,
        meta: {
            breadcrumb: [{
                    name: '<i class="fas fa-home"></i>',
                    link: '/Home'
                },
                {
                    name: 'ตะกร้า',
                    link: '/Basket'
                }
            ]
        }
    },
    {
        path: "/DataUser",
        component: DataUser,
        meta: {
            breadcrumb: [{
                    name: '<i class="fas fa-home"></i>',
                    link: '/Home'
                },
                {
                    name: 'ข้อมูลผู้สมัคร',
                    link: 'DataUser'
                }
            ]
        }
    },
    {
        path: "/DataUserExam",
        component: DataUserUnknow,
        meta: {
            breadcrumb: [{
                    name: '<i class="fas fa-home"></i>',
                    link: '/Home'
                },
                {
                    name: 'ข้อมูลผู้สมัคร',
                    link: 'DataUserExam'
                }
            ]
        }
    },
    {
        path: "/HowPayment",
        component: HowPayment,
        meta: {
            breadcrumb: [{
                    name: '<i class="fas fa-home"></i>',
                    link: '/Home'
                },
                {
                    name: 'ชำระเงิน',
                    link: '/HowPayment'
                }
            ]
        }
    },
    {
        path: "/HowPaymentExam",
        component: HowPaymentUnknow,
        meta: {
            breadcrumb: [{
                    name: '<i class="fas fa-home"></i>',
                    link: '/Home'
                },
                {
                    name: 'ชำระเงิน',
                    link: '/HowPaymentExam'
                }
            ]
        }
    },
    {
        path: "/Profile",
        component: Profile,
        meta: {
            breadcrumb: [{
                    name: '<i class="fas fa-home"></i>',
                    link: '/Home'
                },
                {
                    name: 'profile',
                    link: '/Profile'
                }
            ]
        }
    },
    {
        path: "/EditProfile",
        component: EditProfile,
        meta: {
            breadcrumb: [{
                    name: '<i class="fas fa-home"></i>',
                    link: '/Home'
                },
                {
                    name: 'แก้ไขโปรไฟล์',
                    link: '/EditProfile'
                }
            ],
            auth: true
        }
    },
    {
        path: "/resetPassword",
        component: resetPassword,
        meta: {
            breadcrumb: [{
                    name: '<i class="fas fa-home"></i>',
                    link: '/Home'
                },
                {
                    name: 'เปลี่ยนรหัสผ่าน',
                    link: '/resetPassword'
                }
            ],
            auth: true
        }
    },
    {
        name: "/examContent",
        path: "/examContent/:id",
        component: examContent,
        props: {
            id: 0
        },
        meta: {
            breadcrumb: [{
                    name: '<i class="fas fa-home"></i>',
                    link: '/Home'
                },
                {
                    name: 'ExamContent',
                    link: '/examContent'
                }
            ]
        }
    },
    {
        path: "/Search",
        component: search,
        meta: {
            breadcrumb: [{
                    name: '<i class="fas fa-home"></i>',
                    link: '/Home'
                },
                {
                    name: 'ค้นหา',
                    link: '/search'
                }
            ]
        }
    }
]

const AllRouter = _.concat(RouterAdmin, RouterUser)
export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: AllRouter,
    scrollBehavior(to, from, savedPosition) {
        return {
            x: 0,
            y: 0
        }
    },

})
