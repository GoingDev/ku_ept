require('./bootstrap');

import Vue from 'vue';
import router from './router'
import VueRouter from 'vue-router';
import store from './store'
import VueSweetalert2 from 'vue-sweetalert2'
import _ from 'lodash'
import VueCsrf from 'vue-csrf'
import VeeValidate from 'vee-validate';
import Nav from './user/components/Nav.vue';
import Footer from './user/components/Footer.vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import FBSignInButton from 'vue-facebook-signin-button'
import VueSession from 'vue-session'
import VuePaginate from 'vue-paginate'

Vue.use(VueAxios, axios);
Vue.use(VueRouter);
Vue.use(VueSession)
axios.defaults.baseURL = '/api';
//axios.defaults.baseURL = 'http://phpstack-155228-700635.cloudwaysapps.com/api';
Vue.use(FBSignInButton)
Vue.use(VueCsrf);
Vue.use(VueSweetalert2);
Vue.use(_);
Vue.use(VeeValidate);
Vue.router = router
Vue.use(require('@websanova/vue-auth'), {
    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
});
Vue.use(VuePaginate)



const app = new Vue({
    el: '#app',
    prototype: {
        $http: window.axios
    },
    router,
    store,
    components: {
        'nav-bar': Nav,
        'footer-bar': Footer
    },
    data: {
        active: false,
        showMobileMenu: false
    },
    computed: {
        path() {
            return this.$route.path
        },
        getCategory() {
            let store = this.$store.state.moduleProduct.category;
            return store;
        }
    },
    watch: {
        '$route'(val) {
            this.beforeRouteUpdate();
        }
    },
    mounted() {
        this.$nextTick(() => {
            this.active = true;
        })
    },
    methods: {
        initMobileMenu() {
            const vm = this;
            if ($('#menu').hasClass('mm-menu')) {
                return false;
            } else {
                $('#menu').mmenu({
                    "navbars": [{
                        "position": "bottom",
                        "content": [
                            "<a href='https://th-th.facebook.com/KULanguageCentre/' target='_blank'><i class='fab fa-facebook-f'></i></a>",
                            "<a href='#/' target='_blank'><i class='fab fa-line'></i></a>",
                        ]
                    }]
                });
                this.showMobileMenu = true;
            }

        },
        beforeRouteUpdate() {
            $('#menu').data("mmenu").close();
        }
    },
    updated() {
        const vm = this;
        if (this.$auth.check() || this.$session.id()) {
            if (this.$auth.check()) {
                vm.$store.commit('setUser', this.$auth.user());
            } else {

            }
        } else {
            this.$session.start();
        }
        if (this.$refs.subMenuMobile.children.length > 0) {
            vm.initMobileMenu();
        }
    },
    beforeCreate() {
        this.$store.dispatch('getAllCategory');
        this.$store.dispatch('getAllCourse');
        this.$store.dispatch('getAllExam');
        this.$store.dispatch('getAllPromotion');
        this.$store.dispatch('getAllFaq');
        this.$store.dispatch('getAllContent');
        this.$store.dispatch('getAllNews');
        this.$store.dispatch('getAllGallary');
        this.$store.dispatch('getAllGallaryDetail');
        this.$store.dispatch('getAllSlider');
        this.$store.dispatch('getAllBanner');
        this.$store.dispatch('getAllCart');
        this.$store.dispatch('getAllAddress');
        this.$store.dispatch('getAllOtherNews');
        this.$store.dispatch('getAllOrder');
        this.$store.dispatch('getAllOrderDetail');
    },

});
