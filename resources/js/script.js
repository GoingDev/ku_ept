jQuery(document).ready(function($) {
    console.log('JS');
   


    // remove Loader
    
    $( "#main-loader" ).fadeOut( "fast", function() {
        $('#main-loader').remove();
      });
      

    // toggle Board
    const $listBoardItem = $('.hasTable')
            $toggleTable = $('.toggle-table');
        $listBoardItem.eq(0).addClass('active');
        $toggleTable.eq(0).addClass('active');
    $listBoardItem.click(function(e){
        e.stopPropagation();
        $listBoardItem.removeClass('active');
        $toggleTable.removeClass('active');
        $(this).addClass('active');
        $toggleTable.eq($(this).index()).addClass('active');
    });




    //tabhome
    const $homeTabItem = $('p.homeTab_item'),
            $wrapHomeTabContent = $('.wrap-homeTab-content');
    $wrapHomeTabContent.removeClass('active').eq(0).addClass('active');
    $homeTabItem.removeClass('active').eq(0).addClass('active');
    
    $homeTabItem.click(function(e){
        e.stopPropagation();
        const $this = $(this),
                $thisData = $this.data().tab;
        $homeTabItem.removeClass('active');
        $wrapHomeTabContent.removeClass('active');
        $this.addClass('active');
        $wrapHomeTabContent.each(function(index,el){
            if($(this).data().tab == $thisData){
                $(this).addClass('active');
                return false;
            }
        });

    });


    // tab mini
    const $homeTabItemMini = $('.tabHome-mini'),
        $tabMiniTabHome = $('.tabHome-content-mini');
    $homeTabItemMini.removeClass('active').eq(0).addClass('active');
    $tabMiniTabHome.removeClass('active').eq(0).addClass('active');

    $homeTabItemMini.click(function(e){
        e.stopPropagation();
        const $this = $(this),
            $thisData = $this.data().tab;
        
        $tabMiniTabHome.removeClass('active');
        $homeTabItemMini.removeClass('active');
        $this.addClass('active');
        $tabMiniTabHome.each(function(index,el){
            if($(this).data().tab == $thisData){
                $(this).addClass('active');
                return false;
            }
        });
    });



    // hover google
    const $cardContact = $('.card-contact');
    $('.map-g').hover(function(e){
        $cardContact.addClass('fade');
    },function(e){
        $cardContact.removeClass('fade');
    });


   


    // // slider Home
    // $('#sliderHome').slick({
    //     arrows: false,
    //     dots: true,
    //     appendDots: $('#renderDotsHome'),
    //     dotsClass: 'homeDots',
    //     pauseOnHover: true

    // });

   




      const $howPaymentToggle = $('.howPayment-toggle'),
                $howPaymentBodyToggle = $('.howPayment-body-toggle');
            $howPaymentToggle.removeClass('active').eq(0).addClass('active');
            $howPaymentBodyToggle.removeClass('active').eq(0).addClass('active');
            $howPaymentToggle.click(function(e){
                e.stopPropagation();
                $howPaymentToggle.removeClass('active');
                $howPaymentBodyToggle.removeClass('active');
                $(this).addClass('active');
                $howPaymentBodyToggle.eq($(this).index()).addClass('active');
            });


            const $profileHistoryList = $('.profile-history-list'),
            $basketList = $('.basket-list');
    $profileHistoryList.click(function(e){
        console.log($(this).next());
        $basketList.removeClass('active');
        $profileHistoryList.removeClass('active');
        $(this).addClass('active');
        $(this).next().addClass('active');
    });

});