require('./bootstrap');

import Vue from 'vue';
import router from './router'
import store from './store'
import VueSweetalert2 from 'vue-sweetalert2'
import _ from 'lodash'
import VTooltip from 'v-tooltip'
import Vuetify from 'vuetify'
import VeeValidate from 'vee-validate';
import Nav from './admin/components/NavBar.vue';
import Side from './admin/components/SideBar.vue';
import Footer from './admin/components/FooterBar.vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import VueHtmlToPaper from 'vue-html-to-paper';
import JsonExcel from 'vue-json-excel'

Vue.component('downloadExcel', JsonExcel)
const options = {
  name: '_blank',
  specs: [
    'fullscreen=yes',
    'titlebar=yes',
    'scrollbars=yes'
  ],
  styles: [
    'https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.min.css',
    'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons',
    'https://unpkg.com/kidlat-css/css/kidlat.css'
  ]
}
Vue.use(VueHtmlToPaper, options);
Vue.use(VueAxios, axios);
//axios.defaults.baseURL = 'http://phpstack-155228-700635.cloudwaysapps.com/api';
axios.defaults.baseURL = '/api';
Vue.use(VueSweetalert2);
Vue.use(VTooltip)
Vue.use(_);
Vue.use(Vuetify);
Vue.use(VeeValidate);

import 'vuetify/dist/vuetify.min.css'
import WebFontLoader from 'webfontloader'
const app = new Vue({
    el: '#app',
    prototype: {
        $http: window.axios
    },
    router,
    store,
    components: {
        'nav-bar': Nav,
        'side-bar': Side,
        'footer-bar': Footer,
    },
    data: {
        drawer: true,
        active: false,
    },
    mounted() {
        WebFontLoader.load({
            google: {
                families: ['Roboto:100,300,400,500,700,900']
            },
            active: this.setFontLoaded
        })
        this.$nextTick(()=>{
            this.active = true;
        })
    },
    methods: {
        setFontLoaded() {
            this.$emit('font-loaded')
        }
    },
    beforeCreate() {
        this.$store.dispatch('getAllCategory');
        this.$store.dispatch('getAllCourse');
        this.$store.dispatch('getAllExam');
        this.$store.dispatch('getAllPromotion');
        this.$store.dispatch('getAllFaq');
        this.$store.dispatch('getAllContent');
        this.$store.dispatch('getAllNews');
        this.$store.dispatch('getAllGallary');
        this.$store.dispatch('getAllGallaryDetail');
        this.$store.dispatch('getAllSlider');
        this.$store.dispatch('getAllBanner');
        this.$store.dispatch('getAllOtherNews');
        this.$store.dispatch('getAllAddress');
        this.$store.dispatch('getAllOrder');
        this.$store.dispatch('getAllOrderDetail');
        this.$store.dispatch('getAllMember');
        this.$store.dispatch('getOrderForView');
    }
});



