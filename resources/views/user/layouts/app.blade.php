<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->
    {{-- <meta name="csrf-token" content="{{ csrf_token() }}"> --}}
    <!-- file CSS -->
    <link href="https://fonts.googleapis.com/css?family=Prompt:200,400,500" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />

    <link rel="stylesheet" href="{{ asset('/css/jquery.mmenu.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/jquery.mmenu.navbars.css') }}">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.4/dist/jquery.fancybox.min.css" />

    <link rel="stylesheet" href="{{ asset('/css/fontawesome/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <script>
        window.fbAsyncInit = function () {
            FB.init({
                appId: '960660550800308',
                cookie: true, // enable cookies to allow the server to access the session
                xfbml: true, // parse social plugins on this page
                version: 'v3.2' // use graph api version 2.8
            });
        };
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

    </script>
    <title>Hunman language</title>
    <style>
        .activeShow{
            display: block!important;
        }
    </style>
</head>

<body>
    {{-- <div id="main-loader">
        <div class="lds-ellipsis">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div> --}}

    <div id="app" style="display:none" :class="{ 'activeShow': active == true }">
        <div id="page">
            <nav-bar></nav-bar>
            <transition  name="fade" mode="out-in">
                <router-view></router-view>
            </transition>

            <footer-bar></footer-bar>
        </div>
        <!-- Main Menu Mobile -->
        <nav id="menu" v-show="showMobileMenu">
            <ul>
                <li>
                    <router-link tag="a" to="/Home">
                        หน้าแรก
                    </router-link>
                </li>
                <li>
                    <router-link tag="a" to="/About">
                        เกี่ยวกับเรา
                    </router-link>
                </li>
                <li>
                    <span>หลักสูตรอบรม</span>
                    <ul ref="subMenuMobile">
                        <li v-for="(item,i) in getCategory" :key="i">
                            <router-link tag="a" :to="{name:'/Course/' , params : {id:item.c_id}}">
                                @{{item.c_name}}
                            </router-link>
                        </li>
                    </ul>
                </li>
                <li>
                    <router-link tag="a" to="/Register">
                        สมัครหลักสูตร
                    </router-link>
                </li>
                <li>
                    <router-link tag="a" to="/News">
                        ข่าวสาร/กิจกรรม
                    </router-link>
                </li>
                <li>
                    <router-link tag="a" to="/Gallery">
                        แกลอรี่ทั้งหมด
                    </router-link>
                </li>
                <li>
                    <router-link tag="a" to="/Contact">
                        ติดต่อเรา
                    </router-link>
                </li>
                <li>
                    <router-link tag="a" to="/Login" v-if="!$auth.check()">
                        เข้าสู่ระบบ
                    </router-link>
                    <router-link tag="a" to="/Profile" v-else>
                        <p>@{{$auth.user().name}}</p>
                    </router-link>
                </li>
                <li>
                    <router-link tag="a" to="/Search">
                        ค้นหา 
                    </router-link>
                </li>
            </ul>
        </nav>
    </div>

    <script src="{{ asset('/js/user.js') }}" type="text/javascript"></script>

    <!-- script JS -->

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>



    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.4/dist/jquery.fancybox.min.js"></script>
    <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
    <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>

    <script src="{{ asset('/js/jquery.mmenu.js') }}"></script>
    <script src="{{ asset('/js/jquery.mmenu.navbars.js') }}"></script>

    <script src="{{ asset('/js/script.js') }}"></script>



</body>

</html>
