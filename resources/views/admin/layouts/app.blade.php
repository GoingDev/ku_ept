<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>KU</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' rel="stylesheet">
</head>
<style>
    .activeDisplay{
        display: flex!important;
    }
</style>
<body>
    @if(Auth::user()->status!=1)
        <script>window.location = "/logout";</script>
    @else
    <div id="app">
        <v-app id="inspire">
            <side-bar v-bind:drawer="drawer"></side-bar>
            <nav-bar v-on:switch="drawer=!drawer"></nav-bar>

            <v-content style="display:none" :class="{activeDisplay : active}">
                <v-container fluid>
                    <v-layout justify-center>
                        <router-view></router-view>
                    </v-layout>
                </v-container>
            </v-content>
            <footer-bar></footer-bar>
        </v-app>
    </div>
    @endif
<script src="{{ asset('/js/app.js') }}" type="text/javascript"></script>
</body>
</html>
