<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsContent extends Model
{
    //
    protected $fillable = [
        'nt_name','nt_img','nt_status','nt_text'
    ];
}
