<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Course;
use App\Exam;
use App\Promotion;
use Carbon\Carbon;
use App\Cart;
class UserController extends Controller
{
    //
    public function index(){
        $CourseUnit = Course::whereColumn('course_unit','>=','course_people')->update(['course_status'=>1]);
        $exUnit = Exam::whereColumn('ex_unit','>=','ex_people')->where('ex_limit',0)->update(['ex_status'=>1]);
        $UpdateCourse = Course::whereDate('course_datestart','<', Carbon::today())->update(['course_status'=>1]);
        $UpdateEx = Exam::whereDate('ex_dateend','<', Carbon::today())->update(['ex_status'=>1]);
        $UpdatePromotion = Promotion::whereDate('pro_dateend','<', Carbon::today())->update(['pro_status'=>0]);
        $updateCart = Cart::join('courses', function($join){
            $join->on('carts.course_id','=','courses.course_id')
            ->whereDate('courses.course_datestart','<', Carbon::today());})
            ->delete();
        return view('user.layouts.app');
    }
    public function redirect(){
        return redirect('/Home');
    }
}
