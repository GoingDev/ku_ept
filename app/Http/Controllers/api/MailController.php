<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use Mail;
use App\Http\Controllers\Controller;

class MailController extends Controller
{
    //
    public function sendMail(Request $request){
        $data = $request->params;
        $name = $request->params['name'];
        $email = $request->params['email'];
        $subject = $request->params['subject'];
        $detail = $request->params['detail'];
        Mail::send('email.contact',
            array(
                'name' => $name,
                'email' => $email,
                'user_message' => $detail,
                'subject' => $subject
            ), function($message) use ($data)
        {
            $message->from($data['email'],$data['name']);
            $message->to('thanawat.d@gmail.com')->subject($data['subject']);
        });

        return response()->json(['success'=>'done']);
    }
}
