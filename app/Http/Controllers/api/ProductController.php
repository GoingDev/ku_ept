<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Course;
use App\Exam;
use App\Promotion;
use App\Cart;
use App\Order;
use App\orderDetail;
use App\User;
class ProductController extends Controller
{
    //
    public function getAllCategory(){
        $cat = Category::all();
        return $cat;
    }
    public function AddNewCate(Request $request){
        $c_name = $request->c_name;
        $cat = new Category;
        $cat->c_name = $c_name;
        $cat->save();
        return response()->json(['success'=>'done']);
    }
    public function getAllCourse(){
        return Course::orderBy('course_id','DESC')->get();
    }
    public function AddNewCourse(Request $request){
        $file = $request->file;
        $img = null;
        if($request->hasFile('file')){
            $ex1 = explode(".",$file->getClientOriginalName());
            $fileName = time().$this->clean(base64_encode($ex1[0])).".jpg";
            $file->move(public_path('CoursePicture'), $fileName);
            $img = $fileName;
        }
        $form = json_decode($request->form);
        $course = new Course;
        $course->course_name = $form->name;
        $course->course_description = $form->description;
        $course->course_price = $form->price;
        $course->course_people = $form->people;
        $course->course_status = $form->status;
        $course->c_id = $form->category;
        $course->course_img = $img;
        $course->course_title = $form->title;
        $course->course_datestart = $form->startdate;
        $course->course_dateend = $form->enddate;
        $course->course_timestart = $form->timestart."00";
        $course->course_timeend = $form->timeend."00";
        $course->save();
        return response()->json(['success'=>'done']);
    }
    public function DeleteCourse($id){
        Course::where('course_id',$id)->delete();
        return response()->json(['success'=>'done']);
    }
    public function EditCourse(Request $request){
        $file = $request->file;
        $form = json_decode($request->form);
        if($request->hasFile('file')){
            $ex1 = explode(".",$file->getClientOriginalName());
            $fileName = time().$this->clean(base64_encode($ex1[0])).".jpg";
            $file->move(public_path('CoursePicture'), $fileName);
            $img = $fileName;
            Course::where('course_id',$form->course_id)->update(['course_name'=>$form->course_name,'course_description'=>$form->course_description,
                                                                    'course_price'=>$form->course_price,'course_people'=>$form->course_people,'course_status'=>$form->course_status,
                                                                    'c_id'=>$form->c_id,'course_datestart'=>$form->course_datestart,'course_dateend'=>$form->course_dateend,
                                                                    'course_timestart'=>$form->course_timestart."00",'course_timeend'=>$form->course_timeend."00",'course_img'=>$img,
                                                                    'course_title'=>$form->course_title]);
        }
        else{
            Course::where('course_id',$form->course_id)->update(['course_name'=>$form->course_name,'course_description'=>$form->course_description,
                                                                    'course_price'=>$form->course_price,'course_people'=>$form->course_people,'course_status'=>$form->course_status,
                                                                    'c_id'=>$form->c_id,'course_datestart'=>$form->course_datestart,'course_dateend'=>$form->course_dateend,
                                                                    'course_timestart'=>$form->course_timestart."00",'course_timeend'=>$form->course_timeend."00",
                                                                    'course_title'=>$form->course_title]);
        }
        return response()->json(['success'=>'done']);
    }
    public function getAllExam(){
        return Exam::join('categories','exams.c_id','=','categories.c_id')
                        ->orderBy('ex_status','DESC')
                        ->orderBy('ex_id','DESC')
                        ->get();
    }
    public function AddNewExam(Request $request){
        $form = json_decode($request->form);
        $ex = new Exam;
        $ex->ex_name = $form->name;
        $ex->c_id = $form->category;
        $ex->ex_type = $form->type;
        $ex->ex_limit = $form->limit;
        $ex->ex_people = $form->people;
        $ex->ex_price = $form->price;
        $ex->ex_datestart = $form->startdate;
        $ex->ex_dateend = $form->enddate;
        $ex->ex_timeexam_start = $form->timestart."00";
        $ex->ex_timeexam_end = $form->timeend."00";
        $ex->ex_dateexam = $form->dateexam;
        $ex->ex_emailto_seat = $form->email_seat;
        $ex->ex_emailto_result = $form->email_result;
        $ex->ex_status = $form->status;
        $ex->save();
        return response()->json(['success'=>'done']);
    }
    public function DeleteExam($id){
        Exam::where('ex_id',$id)->delete();
        return response()->json(['success'=>'done']);
    }
    public function EditExam(Request $request){
        $form = json_decode($request->form);
        Exam::where('ex_id',$form->ex_id)->update(['ex_name'=>$form->ex_name,'c_id'=>$form->c_id,'ex_type'=>$form->ex_type,'ex_limit'=>$form->ex_limit,
                                                    'ex_people'=>$form->ex_people,'ex_price'=>$form->ex_price,'ex_datestart'=>$form->ex_datestart,
                                                    'ex_dateend'=>$form->ex_dateend,'ex_timeexam_start'=>$form->ex_timeexam_start.'00',
                                                    'ex_timeexam_end'=>$form->ex_timeexam_end.'00','ex_dateexam'=>$form->ex_dateexam,'ex_status'=>$form->ex_status,
                                                    'ex_emailto_seat'=>$form->ex_emailto_seat,'ex_emailto_result'=>$form->ex_emailto_result]);

        return response()->json(['success'=>'done']);
    }
    public function getAllPromotion(){
        return Promotion::orderBy('pro_id','DESC')->get();
    }
    public function AddNewPromotion(Request $request){
        $form = json_decode($request->form);
        $pro = new Promotion;
        $pro->pro_code = $form->code;
        $pro->pro_type = $form->type;
        $pro->pro_discount = $form->discount;
        $pro->pro_datestart = $form->datestart;
        $pro->pro_dateend = $form->dateend;
        $pro->pro_status = $form->status;
        $pro->save();
        return response()->json(['success'=>'done']);
    }
    public function DeletePromotion($id){
        Promotion::where('pro_id',$id)->delete();
        return response()->json(['success'=>'done']);
    }
    public function ChangeStatusPromotion(Request $request){
        Promotion::where('pro_id',$request->id)->update(['pro_status'=>$request->status]);
        return response()->json(['success'=>'done']);
    }
    public function getAllCart(){
        return Cart::join('courses','courses.course_id','=','carts.course_id')->get();
    }
    public function AddtoCart(Request $request){
        $data = $request->params;
        Cart::create($data);
        return response()->json(['success'=>'done']);
    }
    public function RemoveCart($id){
        Cart::where('cart_id',$id)->delete();
        return response()->json(['success'=>'done']);
    }
    public function getAllOrder(){
        return Order::all();
    }
    public function getAllOrderDetail(){
        $exam = orderDetail::join('exams','order_details.exam_id','=','exams.ex_id')->get();
        $course = orderDetail::join('courses','order_details.course_id','=','courses.course_id')->get();
        return response()->json(['exam'=>$exam,'course'=>$course]);
    }
    public function getAllMember(){
        return User::where('id','!=',1)->get();
    }
    public function getOrderForView(){
        return orderDetail::join('orders','order_details.order_id','=','orders.order_id')->get();
    }
    public function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }
}
