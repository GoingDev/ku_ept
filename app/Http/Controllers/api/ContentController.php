<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Faq;
use App\Content;
use App\NewsContent;
use App\Gallary;
use App\GallaryDetail;
use App\SliderHome;
use App\Banner;
use App\User;
use App\otherNews;
use Hash;
class ContentController extends Controller
{
    //
    public function getAllFaq(){
        return Faq::all();
    }
    public function AddNewFaq(Request $request){
        $form = json_decode($request->form);
        $faq = new Faq;
        $faq->faq_issue = $form->issue;
        $faq->faq_text = $form->text;
        $faq->save();
        return response()->json(['success'=>'done']);
    }
    public function DeleteFaq($id){
        Faq::where('faq_id',$id)->delete();
        return response()->json(['success'=>'done']);
    }
    public function EditFaq(Request $request){
        $form = json_decode($request->form);
        Faq::where('faq_id',$form->id)->update(['faq_issue'=>$form->issue,'faq_text'=>$form->text]);
        return response()->json(['success'=>'done']);
    }
    public function EditRegis(Request $request){
        $msg = $request->msg;
        Content::where('ct_id',1)->update(['ct_text'=>$msg]);
        return response()->json(['success'=>'done']);
    }
    public function getAllContent(){
        return Content::all();
    }
    public function EditContent(Request $request){
        $form = json_decode($request->form);
        Content::where('ct_id',$form->id)->update(['ct_text'=>$form->text]);
        return response()->json(['success'=>'done']);
    }
    public function getAllNews(Request $request){
        return NewsContent::orderBy('nt_id','DESC')->get();
    }
    public function AddNewBlog(Request $request){
        $file = $request->file;
        $img = null;
        if($request->hasFile('file')){
            $ex1 = explode(".",$file->getClientOriginalName());
            $fileName = time().$this->clean(base64_encode($ex1[0])).".jpg";
            $file->move(public_path('NewsPicture'), $fileName);
            $img = $fileName;
        }
        $form = json_decode($request->form);
        $news = new NewsContent;
        $news->nt_name = $form->name;
        $news->nt_text = $form->text;
        $news->nt_status = $form->status;
        $news->nt_img = $img;
        $news->save();
        return response()->json(['success'=>'done']);
    }
    public function DeleteBlog($id){
        NewsContent::where('nt_id',$id)->delete();
        return response()->json(['success'=>'done']);
    }
    public function EditBlog(Request $request){
        $file = $request->file;
        $form = json_decode($request->form);
        if($request->hasFile('file')){
            $ex1 = explode(".",$file->getClientOriginalName());
            $fileName = time().$this->clean(base64_encode($ex1[0])).".jpg";
            $file->move(public_path('NewsPicture'), $fileName);
            $img = $fileName;
            NewsContent::where('nt_id',$form->nt_id)->update(['nt_name'=>$form->nt_name,'nt_text'=>$form->nt_text,
                                                                'nt_status'=>$form->nt_status,'nt_img'=>$img]);
        }
        else{
            NewsContent::where('nt_id',$form->nt_id)->update(['nt_name'=>$form->nt_name,'nt_text'=>$form->nt_text,
                                                                'nt_status'=>$form->nt_status]);
        }
        return response()->json(['success'=>'done']);
    }
    public function AddNewGallary(Request $request){
        $form = json_decode($request->form);
        $formGallary = $request->gallary;
        $gallary = new Gallary;
        $gallary->g_name = $form->name;
        $gallary->g_description = $form->text;
        $gallary->g_status = $form->status;
        $gallary->save();
        $id = $gallary->id;
        try{
            foreach($formGallary as $row){
                $ex1 = explode(".",$row->getClientOriginalName());
                $fileName = time().$this->clean(base64_encode($ex1[0])).".jpg";
                $fileSize = $row->getClientSize();
                $row->move(public_path('Gallary'), $fileName);
                $gd = new GallaryDetail;
                $gd->gd_img = $fileName;
                $gd->gd_size = $fileSize;
                $gd->g_id = $id;
                $gd->save();
            }
        }
        catch(\Exception $e){

        }
        return response()->json(['success'=>'done']);
    }
    public function getAllGallary(Request $request){
        return Gallary::all();
    }
    public function getAllGallaryDetail(Request $request){
        return GallaryDetail::all();
    }
    public function DeleteGallary($id){
        Gallary::where('g_id',$id)->delete();
        GallaryDetail::where('g_id',$id)->delete();
        return response()->json(['success'=>'done']);
    }
    public function EditGallary(Request $request){
        $form = json_decode($request->form);
        $newGallary = $request->newGallary;
        $oldGallary = $request->oldGallary;
        Gallary::where('g_id',$form->id)->update(['g_name'=>$form->name,'g_description'=>$form->text,
                                                    'g_status'=>$form->status]);
        GallaryDetail::where('g_id',$form->id)->delete();
        try{
            foreach($newGallary as $row){
                    $ex1 = explode(".",$row->getClientOriginalName());
                    $fileName = time().$this->clean(base64_encode($ex1[0])).".jpg";
                    $fileSize = $row->getClientSize();
                    $row->move(public_path('Gallary'), $fileName);
                    $gd = new GallaryDetail;
                    $gd->gd_img = $fileName;
                    $gd->gd_size = $fileSize;
                    $gd->g_id = $form->id;
                    $gd->save();
                }
        }
        catch(\Exception $e){ }
        try{
            foreach($oldGallary as $row2){
                $file = json_decode($row2);
                $gd = new GallaryDetail;
                $gd->gd_img = $file->name;
                $gd->gd_size = $file->size;
                $gd->g_id = $form->id;
                $gd->save();
            }
        }
        catch(\Exception $e){

        }
        return response()->json(['success'=>'done']);
    }
    public function AddNewSlider(Request $request){
        $form = json_decode($request->form);
        $img = null;
        if($request->hasFile('file')){
            $file = $request->file;
            $ex1 = explode(".",$file->getClientOriginalName());
            $fileName = time().$this->clean(base64_encode($ex1[0])).".jpg";
            $file->move(public_path('SliderPicture'), $fileName);
            $img = $fileName;
        }
        $slider = new SliderHome;
        $slider->sli_name = $form->name;
        $slider->sli_img = $img;
        $slider->sli_title = $form->title;
        $slider->sli_subtitle = $form->subtitle;
        $slider->sli_headtitle = $form->headtitle;
        $slider->sli_btn1_link = $form->btn1_link;
        $slider->sli_btn1_text = $form->btn1_text;
        $slider->sli_btn2_link = $form->btn2_link;
        $slider->sli_btn2_text = $form->btn2_text;
        $slider->sli_status = $form->status;
        $slider->save();
        return response()->json(['success'=>'done']);
    }
    public function getAllSlider(){
        return SliderHome::orderBy('sli_id','ASC')->get();
    }
    public function DeleteSlider($id){
        SliderHome::where('sli_id',$id)->delete();
    }
    public function EditSlider(Request $request){
        $form = json_decode($request->form);
        $img = null;
        if($request->hasFile('file')){
            $file = $request->file;
            $ex1 = explode(".",$file->getClientOriginalName());
            $fileName = time().$this->clean(base64_encode($ex1[0])).".jpg";
            $file->move(public_path('SliderPicture'), $fileName);
            $img = $fileName;
            SliderHome::where('sli_id',$form->id)->update(['sli_name'=>$form->name,'sli_img'=>$img,'sli_title'=>$form->title,
                                                        'sli_subtitle'=>$form->subtitle,'sli_headtitle'=>$form->headtitle,
                                                        'sli_btn1_link'=>$form->btn1_link,'sli_btn1_text'=>$form->btn1_text,
                                                        'sli_btn2_link'=>$form->btn2_link,'sli_btn2_text'=>$form->btn2_text,
                                                        'sli_status'=>$form->status]);
        }
        else{
            SliderHome::where('sli_id',$form->id)->update(['sli_name'=>$form->name,'sli_title'=>$form->title,
                                                        'sli_subtitle'=>$form->subtitle,'sli_headtitle'=>$form->headtitle,
                                                        'sli_btn1_link'=>$form->btn1_link,'sli_btn1_text'=>$form->btn1_text,
                                                        'sli_btn2_link'=>$form->btn2_link,'sli_btn2_text'=>$form->btn2_text,
                                                        'sli_status'=>$form->status]);
        }
        return response()->json(['success'=>'done']);
    }
    public function UpdateIndex(Request $request){
        $form = json_decode($request->form);
        SliderHome::truncate();
        foreach($form as $row){
            $slider = new SliderHome;
            $slider->sli_name = $row->sli_name;
            $slider->sli_img = $row->sli_img;
            $slider->sli_title = $row->sli_title;
            $slider->sli_subtitle = $row->sli_subtitle;
            $slider->sli_headtitle = $row->sli_headtitle;
            $slider->sli_btn1_link = $row->sli_btn1_link;
            $slider->sli_btn1_text = $row->sli_btn1_text;
            $slider->sli_btn2_link = $row->sli_btn2_link;
            $slider->sli_btn2_text = $row->sli_btn2_text;
            $slider->sli_status = $row->sli_status;
            $slider->save();
        }
        return response()->json(['success'=>'done']);
    }
    public function getAllBanner(){
        return Banner::all();
    }
    public function EditBanner(Request $request){
        $form = json_decode($request->form);
        $img = null;
        if($request->hasFile('file')){
            $file = $request->file;
            $ex1 = explode(".",$file->getClientOriginalName());
            $fileName = time().$this->clean(base64_encode($ex1[0])).".jpg";
            $file->move(public_path('BannerPicture'), $fileName);
            $img = $fileName;
            Banner::where('banner_id',$form->id)->update(['banner_img'=>$img,'banner_title'=>$form->title,
                                                        'banner_subtitle'=>$form->subtitle]);
        }
        else{
            Banner::where('banner_id',$form->id)->update(['banner_title'=>$form->title,'banner_subtitle'=>$form->subtitle]);
        }
        return response()->json(['success'=>'done']);
    }
    public function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }
    public function EditProfile(Request $request){
        $data = $request->params;
        $id = $data['id'];
        User::where('id',$id)->update(['name'=>$data['name'],'name_TH'=>$data['name_TH'],'email'=>$data['email'],'tel'=>$data['tel'],
                                        'address'=>$data['address'],'birthday'=>$data['birthday'],'province'=>$data['province'],
                                        'district'=>$data['district'],'amphur'=>$data['amphur'],'code'=>$data['code'],
                                        'line_id'=>$data['line_id'],'slip_type'=>$data['slip_type'],'address_slip'=>$data['address_slip'],
                                        'prov_slip'=>$data['prov_slip'],'amph_slip'=>$data['amph_slip'],'dist_slip'=>$data['dist_slip'],
                                        'code_slip'=>$data['code_slip']]);
        return User::where('id',$id)->get();
    }
    public function getAllOtherNews(){
        return otherNews::orderBy('othernews_id','DESC')->get();
    }
    public function AddNewOtherNews(Request $request){
        $file = $request->file;
        $img = null;
        if($request->hasFile('file')){
            $ex1 = explode(".",$file->getClientOriginalName());
            $fileName = time().$this->clean(base64_encode($ex1[0])).".jpg";
            $file->move(public_path('OtherNewsPicture'), $fileName);
            $img = $fileName;
        }
        $form = json_decode($request->form);
        $on = new otherNews;
        $on->othernews_cid = $form->category;
        $on->othernews_img = $img;
        $on->othernews_title = $form->title;
        $on->othernews_content = $form->content;
        $on->save();
        return response()->json(['success'=>'done']);
    }
    public function EditOtherNews(Request $request){
        $file = $request->file;
        $form = json_decode($request->form);
        if($request->hasFile('file')){
            $ex1 = explode(".",$file->getClientOriginalName());
            $fileName = time().$this->clean(base64_encode($ex1[0])).".jpg";
            $file->move(public_path('OtherNewsPicture'), $fileName);
            $img = $fileName;
            otherNews::where('othernews_id',$form->othernews_id)->update(['othernews_title'=>$form->othernews_title,'othernews_content'=>$form->othernews_content,
                                                                            'othernews_cid'=>$form->othernews_cid,'othernews_img'=>$fileName]);
        }
        else{
            otherNews::where('othernews_id',$form->othernews_id)->update(['othernews_title'=>$form->othernews_title,'othernews_content'=>$form->othernews_content,
                                                                            'othernews_cid'=>$form->othernews_cid]);
        }
        return response()->json(['success'=>'done']);
    }
    public function DeleteOtherNews($id){
        $del = otherNews::where('othernews_id',$id)->delete();
        return response()->json(['success'=>'done']);
    }
    public function changePassword(Request $request){
        $data = $request->params;
        $u_id = $data['u_id'];
        $opassword = $data['opassword'];
        $npassword = $data['npassword'];
        $user = User::find($u_id);
        if(Hash::check($opassword, $user->password)){
            $user = User::where('id',$u_id)->update(['password'=>Hash::make($npassword)]);
            return response()->json(['success'=>'done']);
        }
        else{
            return response()->json(['err'=>'รหัสผ่านไม่ถูกต้อง']);
        }
    }
}
