<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use JWTAuth;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\RegisterFormRequest;

class AuthController extends Controller
{
    //
    public function register(RegisterFormRequest $request)
    {
        $user = new User;
        $user->email = $request->email;
        $user->name = $request->name;
        $user->password = Hash::make($request->password);
        $user->status = 2;
        $user->save();
        return response([
            'status' => 'success',
            'data' => $user
           ], 200);
    }
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if ( ! $token = JWTAuth::attempt($credentials)) {
                return response([
                    'status' => 'error',
                    'error' => 'invalid.credentials',
                    'msg' => 'Invalid Credentials.'
                ], 400);
        }
        return response([
                'status' => 'success',
                ''
            ])
            ->header('Authorization', $token);
    }
    public function logout()
    {
        JWTAuth::invalidate();
        return response([
                'status' => 'success',
                'msg' => 'Logged out Successfully.'
            ], 200);
    }
    public function user(Request $request)
    {
        $user = User::find(Auth::user()->id);
        return response([
                'status' => 'success',
                'data' => $user
            ]);
    }
    public function refresh()
    {
        return response([
                'status' => 'success'
            ]);
    }

    public function checkFB(Request $request){
        $req = $request->params;
        $name = $req['name'];
        $email = $req['email'];
        $facebookid = $req['id'];
        $user = $this->checkExistUserByFacebookId($facebookid);
	    		if($user != null){
                    return $user;
                }
                else{
                    if($email == null){
                        $email = $facebookid."@facebook.com";
                    }
                    $user = new User;
                    $user->email = $email;
                    $user->password = Hash::make("FB".substr($facebookid,0,6));
                    $user->name = $name;
                    $user->status = 2;
                    $user->facebook_id = $facebookid;
                    $user->save();
                    return $user;
                }
    }
    private function checkExistUserByFacebookId($facebook_id){
        $user = User::where('facebook_id','=',$facebook_id)->first();
        return $user;
    }
}
