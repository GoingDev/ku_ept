<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Exam;
use App\Promotion;
use Carbon\Carbon;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $CourseUnit = Course::whereColumn('course_unit','>','course_people')->update(['course_status'=>1]);
        $exUnit = Exam::whereColumn('ex_unit','>=','ex_people')->where('ex_limit','!=',0)->update(['ex_status'=>1]);
        $UpdateCourse = Course::whereDate('course_datestart','<', Carbon::today())->update(['course_status'=>1]);
        $UpdateEx = Exam::whereDate('ex_dateend','<', Carbon::today())->update(['ex_status'=>1]);
        $UpdatePromotion = Promotion::whereDate('pro_dateend','<', Carbon::today())->update(['pro_status'=>0]);
        return view('admin/layouts/app');
    }
}
