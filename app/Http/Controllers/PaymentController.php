<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Exam;
use App\Course;
use App\orderDetail;
use App\Promotion;
class PaymentController extends Controller
{
    //
    public function payCounterService(){
        /*$onwTwoThreeReq = \Payment2C2P::OneTwoThreeRequest([
            'MessageID' => '222222',
            'InvoiceNo' => 'QW232142',
            'Amount'    => 24444,
            'Discount'    => 10,
            'ShippingFee'    => 10,
            'ServiceFee'    => 10,
            'ProductDesc' => '1 room for 2 nights',
            'PayerName' => 'Name',
            'PayerEmail' => 'email@email.com',
            'ShippingAddress' => 'Yangon',
            'PayInSlipInfo' => 'Hello World',
            'PaymentItems' => [
                [
                    'id' => 1212,
                    'name' => 'Bla Bla',
                    'price' => 12222,
                    'quantity' => 1
                ],
                [
                    'id' => 12,
                    'name' => 'Bla Bla#2',
                    'price' => 12222,
                    'quantity' => 1
                ]
            ]
        ]);
        return view('payCounterService',['req'=>$onwTwoThreeReq]); */
    }
    public function payment_3d(Request $request){
        /*$payload = \Payment2C2P::paymentRequest([
            'desc' => '1 room for 2 nights',
            'uniqueTransactionCode' => "Invoice".time(),
            'amt' => '1000000',
            'currencyCode' => '702',
            'cardholderName' => 'Card holder Name',
            'cardholderEmail' => 'email@emailcom',
            'panCountry' => 'SG',
            'encCardData' => $request->input('encryptedCardInfo'), // Retrieve encrypted credit card data
            'userDefined1' => 'userDefined1',
            'userDefined2' => 'userDefined2'
        ]);
        return view('payCard',['req'=>$payload]);*/
    }
    public function payPlus(Request $request){
        //return view('payPlus');
        $input = $request->input();
        $price = $input['price'];
        $user = json_decode($input['user']);
        $address = $input['address'];
        $addressSlip = $input['addressSlip'];
        $type = $input['type'];
        if($type == 1){ ///exam order
            $allorder = Order::where('order_type',1)->get();
            $countrow = $allorder->count();
            $ordernum = $countrow+1;
            $rule = 8 - strlen($ordernum);
            for($i=0;$i<$rule;$i++){
                $ordernum = "0".$ordernum;
            }
            $exam = $input['exam'];
            $order = new Order;
            $order->order_price = $price;
            $order->order_type = 1;
            $order->order_code = "EX".$ordernum;
            $order->order_address = $address;
            $order->order_address_slip = $addressSlip;
            $order->order_status = 0;
            $order->u_id = $user->id;
            $order->u_name = $user->name;
            $order->u_name_TH = $user->name_TH;
            $order->u_tel = $user->tel;
            $order->u_birthday = $user->birthday;
            $order->u_email = $user->email;
            $order->save();
            $id = $order->id;
            $od = new orderDetail;
            $od->order_id = $id;
            $od->exam_id = $exam;
            $od->save();
            $updateexam = Exam::where('ex_id',$exam)->increment('ex_unit',1);
        }
        else if($type == 2){ ///course order
            $allorder = Order::where('order_type',2)->get();
            $countrow = $allorder->count();
            $ordernum = $countrow+1;
            $rule = 8 - strlen($ordernum);
            for($i=0;$i<$rule;$i++){
                $ordernum = "0".$ordernum;
            }
            $pro_id = $input['pro_id'];
            $promotion = Promotion::where('pro_id',$pro_id)->get();
            $discount = 0;
            $detail = null ;
            if(count($promotion) != 0){
                $promotion = $promotion[0];
                $discount = 0;
                $detail = '' ;
                if($promotion->pro_type == 1){
                    $discount += ($promotion->pro_discount*$price)/100;
                    $detail = 'ส่วนลด' . $promotion->pro_discount . '%';
                }
                else if($promotion->pro_type == 2){
                    $discount += $price - $promotion->pro_discount;
                    $detail = 'ส่วนลด ' . $promotion->pro_discount . ' บาท';
                }
            }
            $cart = explode(",",$input['cart']);
            $order = new Order;
            $order->order_price = $price;
            $order->order_type = 2;
            $order->order_discount = $discount;
            $order->order_discount_detail = $detail;
            $order->order_code = "CS".$ordernum;
            $order->order_address = $address;
            $order->order_address_slip = $addressSlip;
            $order->order_status = 0;
            $order->u_id = $user->id;
            $order->u_name = $user->name;
            $order->u_name_TH = $user->name_TH;
            $order->u_tel = $user->tel;
            $order->u_birthday = $user->birthday;
            $order->u_email = $user->email;
            $order->save();
            $id = $order->id;
            for($i=0;$i<count($cart);$i++){
                $od = new orderDetail;
                $od->order_id = $id;
                $od->course_id = $cart[$i];
                $od->save();
                $updatecourse = Course::where('course_id',$cart[$i])->increment('course_unit',1);
            }
        }
        return redirect('/Home');
    }
}
