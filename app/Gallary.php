<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallary extends Model
{
    //
    protected $fillable = [
        'g_name','g_description','g_staut'
    ];
}
