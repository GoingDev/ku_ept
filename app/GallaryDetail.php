<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GallaryDetail extends Model
{
    //
    protected $fillable = [
        'gd_img','gd_size','g_id'
    ];
}
